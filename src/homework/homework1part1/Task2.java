package homework.homework1part1;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        double rms = Math.pow((a * a + b * b) / 2.0, 0.5);
        System.out.println(rms);
    }
}
