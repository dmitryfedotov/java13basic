package homework.homework1part1;

import java.util.Scanner;

public class Task5 {
    final static double CENTIMETERS_IN_INCH = 2.54;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double count = scanner.nextDouble();
        System.out.println(count * CENTIMETERS_IN_INCH);
    }
}
