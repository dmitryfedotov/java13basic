package homework.homework1part1;

import java.util.Scanner;

public class Task6 {
    final static double KILOMETERS_IN_MILE = 1.60934;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double count = scanner.nextDouble();
        System.out.println(count / KILOMETERS_IN_MILE);
    }
}
