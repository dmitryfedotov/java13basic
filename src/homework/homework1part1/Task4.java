package homework.homework1part1;

import java.util.Scanner;

public class Task4 {
    final static int SECONDS_IN_MINUTE = 60;
    final static int MINUTES_IN_HOUR = 60;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();

        int totalMinutes = count / SECONDS_IN_MINUTE;
        int hours = totalMinutes / MINUTES_IN_HOUR;
        int minutes = totalMinutes % MINUTES_IN_HOUR;
        System.out.println(hours + " " + minutes);
    }
}
