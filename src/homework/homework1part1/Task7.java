package homework.homework1part1;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();

        int firstDigit = count / 10;
        int secondDigit = count % 10;
        System.out.println("" + secondDigit + firstDigit);
    }
}
