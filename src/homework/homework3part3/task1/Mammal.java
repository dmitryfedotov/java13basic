package homework.homework3part3.task1;

public abstract class Mammal extends Animal{
    protected Mammal() {
        super(WaysOfBirth.VIVIPAROUS);
    }
}
