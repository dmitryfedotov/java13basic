package homework.homework3part3.task1;

// Some fish can't swim.
// So, this class will not implement interface Swimming.
public abstract class Fish extends Animal{
    protected Fish() {
        super(WaysOfBirth.SPAWNING);
    }
}
