package homework.homework3part3.task1;

public class AnimalTest {
    public static void main(String[] args) {
        Flying bat = new Bat();
        bat.fly();

        Flying eagle = new Eagle();
        eagle.fly();

        Dolphin dolphin = new Dolphin();
        dolphin.swim();
        dolphin.eat();
        dolphin.sleep();

        GoldFish goldFish = new GoldFish();
        goldFish.swim();
        System.out.println(goldFish.getWayOfBirth());
    }
}
