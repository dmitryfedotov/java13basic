package homework.homework3part3.task1;

public class Dolphin extends Mammal implements Swimming {
    @Override
    public void swim() {
        System.out.println("Wow. Such fast.");
    }
}
