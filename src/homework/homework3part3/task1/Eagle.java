package homework.homework3part3.task1;

public class Eagle extends  Bird implements Flying {
    @Override
    public void fly() {
        System.out.println("I can fly very fast.");
    }
}
