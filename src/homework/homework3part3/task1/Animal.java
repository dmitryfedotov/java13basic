package homework.homework3part3.task1;

public abstract class Animal {
    private final WaysOfBirth wayOfBirth;

    protected Animal(WaysOfBirth wayOfBirth) {
        this.wayOfBirth = wayOfBirth;
    }

    public final void sleep() {
        System.out.println("I'm sleeping.");
    }

    public final void eat() {
        System.out.println("I'm eating.");
    }

    public WaysOfBirth getWayOfBirth() {
        return wayOfBirth;
    }
}
