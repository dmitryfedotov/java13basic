package homework.homework3part3.task1;

public enum WaysOfBirth {
    VIVIPAROUS,
    SPAWNING,
    EGG_LAYING
}
