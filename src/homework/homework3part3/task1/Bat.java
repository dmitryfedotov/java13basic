package homework.homework3part3.task1;

public class Bat extends Mammal implements Flying {
    @Override
    public void fly() {
        System.out.println("I'm flying not so fast.");
    }
}
