package homework.homework3part3.task1;

public abstract class Bird extends Animal {
    protected Bird() {
        super(WaysOfBirth.EGG_LAYING);
    }
}
