package homework.homework3part3.task2;

public class BestCarpenterEver {
    public static void main(String[] args) {
        Stool stool = new Stool();
        Table table = new Table();
        System.out.println(canRepair(stool));
        System.out.println(canRepair(table));
    }
    public static boolean canRepair(Furniture furniture) {
        return furniture instanceof Stool;
    }
}
