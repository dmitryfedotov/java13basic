package homework.homework3part3;

import java.util.ArrayList;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            ArrayList<Integer> row = new ArrayList<>();
            for (int j = 0; j < n; j++) {
                row.add(i + j);
            }
            matrix.add(row);
        }

        for (ArrayList<Integer> row: matrix) {
            for (Integer value: row) {
                System.out.print(value + " ");
            }
            System.out.println();
        }
    }
}
