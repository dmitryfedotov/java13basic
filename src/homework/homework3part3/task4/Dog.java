package homework.homework3part3.task4;

public class Dog {
    private String name;
    private Participant owner;
    private double averageScore;

    public Dog(String name, Participant owner) {
        this.name = name;
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public Participant getOwner() {
        return owner;
    }

    public void setAverageScore(double[] scores) {
        double sum = 0;
        for (int i = 0; i < scores.length; i++) {
            sum += scores[i];
        }

        this.averageScore = ((int) (sum / scores.length * 10) / 10.0);
    }

    public double getAverageScore() {
        return averageScore;
    }
}
