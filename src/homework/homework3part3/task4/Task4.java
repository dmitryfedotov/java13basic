package homework.homework3part3.task4;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        Participant[] owners = new Participant[n];
        for (int i = 0; i < n; i++) {
            owners[i] = new Participant(scanner.next());
        }

        Dog[] dogs = new Dog[n];
        for (int i = 0; i < n; i++) {
            dogs[i] = new Dog(scanner.next(), owners[i]);
        }

        for (int i = 0; i < n; i++) {
            double[] scores = new double[3];
            for (int j = 0; j < 3; j++) {
                scores[j] = scanner.nextDouble();
            }
            dogs[i].setAverageScore(scores);
        }

        Comparator<Dog> comparator = new Comparator<Dog>() {
            @Override
            public int compare(Dog o1, Dog o2) {
                return Double.compare(o2.getAverageScore(), o1.getAverageScore());
            }
        };
        Arrays.sort(dogs, comparator);
        printDog(dogs[0]);
        printDog(dogs[1]);
        printDog(dogs[2]);
    }

    static void printDog(Dog dog) {
        System.out.printf("%s: %s, %s\n",
                dog.getOwner().getName(), dog.getName(), dog.getAverageScore());
    }
}
