package homework.homework1part2;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double n = scanner.nextDouble();

        double diff = Math.log(Math.exp(n)) - n;
        System.out.println(Math.abs(diff) < 1e-6);
    }
}
