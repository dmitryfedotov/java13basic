package homework.homework1part2;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        int index = s.indexOf(" ");
        System.out.println(s.substring(0, index));
        System.out.println(s.substring(index + 1));
    }
}
