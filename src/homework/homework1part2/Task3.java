package homework.homework1part2;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n > 12)
            System.out.println("Пора");
        else
            System.out.println("Рано");
    }
}
