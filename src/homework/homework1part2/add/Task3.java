package homework.homework1part2.add;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String model = scanner.nextLine();
        int price = scanner.nextInt();

        if (model.matches(".*((iphone)|(samsung)).*") && price >= 50000 && price <= 120000)
            System.out.println("Можно купить");
        else
            System.out.println("Не подходит");
    }
}
