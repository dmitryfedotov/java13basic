package homework.homework1part2.add;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String packageDescription = scanner.nextLine();

        boolean rocksInPackage = packageDescription.matches(".*(камни).*");
        boolean forbidden = packageDescription.matches(".*(запрещенная продукция).*");

        if (rocksInPackage && forbidden)
            System.out.println("в посылке камни и запрещенная продукция");
        else if (rocksInPackage)
            System.out.println("камни в посылке");
        else if (forbidden)
            System.out.println("в посылке запрещенная продукция");
        else
            System.out.println("все ок");
    }
}
