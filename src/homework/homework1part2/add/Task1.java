package homework.homework1part2.add;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String password = scanner.nextLine();

        boolean passwordIsCorrect = password.matches(".*[A-Z].*")
                && password.matches(".*[a-z].*")
                && password.matches(".*\\d.*")
                && password.matches(".*[_*-].*");

        if (passwordIsCorrect)
            System.out.println("пароль надежный");
        else
            System.out.println("пароль не прошел проверку");
    }
}
