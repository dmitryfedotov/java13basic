package homework.homework1part2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();

        double diff = Math.pow(Math.sin(x), 2) + Math.pow(Math.cos(x), 2) - 1;
        System.out.println(Math.abs(diff) < 1e-6);
    }
}
