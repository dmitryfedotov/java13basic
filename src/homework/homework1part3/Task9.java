package homework.homework1part3;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int result = 0;
        while (scanner.nextInt() < 0) {
            result++;
        }

        System.out.println(result);
    }
}