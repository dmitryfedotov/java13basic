package homework.homework1part3;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        int result = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' ')
                result++;
        }

        System.out.println(result);
    }
}
