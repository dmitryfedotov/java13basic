package homework.homework1part3;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();

        int result = 0;
        for (int i = 0; i < n; i++) {
            int a = scanner.nextInt();
            if (a > p)
                result += a;
        }
        System.out.println(result);
    }
}
