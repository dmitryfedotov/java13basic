package homework.homework1part3;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        int result = 0;
        for (int i = 1; i <= n; i++) {
            result += (int) Math.pow(m, i);
        }
        System.out.println(result);
    }
}
