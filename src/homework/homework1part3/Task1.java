package homework.homework1part3;

public class Task1 {
    public static void main(String[] args) {
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j < 10; j++) {
                System.out.printf("%s x %s = %s", i, j, i * j);
                System.out.println();
            }
        }
    }
}
