package homework.homework1part3;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        n = getCash(n, 8);
        n = getCash(n, 4);
        n = getCash(n, 2);
        System.out.print(n);

    }

    static int getCash(int sum, int nominal) {
        int result = sum / nominal;
        System.out.print(result + " ");
        return sum - result * nominal;
    }
}
