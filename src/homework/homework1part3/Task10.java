package homework.homework1part3;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int width = n * 2 - 1;
        for (int i = 1; i <= n; i++) {
            int currentWidth = i * 2 - 1;
            int spaces = width - currentWidth;
            printSymbol(" ", spaces / 2);
            printSymbol("#", currentWidth);
            System.out.println();
        }

        printSymbol(" ", (width - 1) / 2);
        System.out.print("|");
    }

    static void printSymbol(String symbol, int count) {
        for (int i = 0; i < count; i++) {
            System.out.print(symbol);
        }
    }
}
