package homework.homework1part3;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        int result = 0;
        for (int i = m; i <= n; i++) {
            result += i;
        }
        System.out.println(result);
    }
}
