package homework.homework2part2;

import java.util.Locale;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.ENGLISH);
        int n = scanner.nextInt();

        String[] owners = new String[n];
        String[] dogs = new String[n];
        double[][] marks = new double[n][3];

        for (int i = 0; i < n; i++) {
            owners[i] = scanner.next();
        }

        for (int i = 0; i < n; i++) {
            dogs[i] = scanner.next();
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                marks[i][j] = scanner.nextDouble();
            }
        }

        double[] averageMarks = new double[n];
        for (int i = 0; i < n; i++) {
            double sum = 0;
            for (int j = 0; j < 3; j++) {
                sum += marks[i][j];
            }
            averageMarks[i] = ((int) (sum / 3 * 10)) / 10.0;
        }

        double firstMark = 0;
        double secondMark = 0;
        double thirdMark = 0;

        for (int i = 0; i < n; i++) {
            if (averageMarks[i] > firstMark) {
                thirdMark = secondMark;
                secondMark = firstMark;
                firstMark = averageMarks[i];
            } else if (averageMarks[i] > secondMark) {
                thirdMark = secondMark;
                secondMark = averageMarks[i];
            } else if (averageMarks[i] > secondMark)
                thirdMark = averageMarks[i];
        }

        String[] placement = new String[3];
        for (int i = 0; i < n; i++) {
            if (averageMarks[i] == firstMark)
                placement[0] = owners[i] + ": " + dogs[i] + ", " + averageMarks[i];
            else if (averageMarks[i] == secondMark)
                placement[1] = owners[i] + ": " + dogs[i] + ", " + averageMarks[i];
            else if (averageMarks[i] == thirdMark)
                placement[2] = owners[i] + ": " + dogs[i] + ", " + averageMarks[i];
        }

        for (int i = 0; i < 3; i++) {
            System.out.println(placement[i]);
        }
    }
}
