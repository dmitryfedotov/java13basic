package homework.homework2part2;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();
        int x2 = scanner.nextInt();
        int y2 = scanner.nextInt();

        int[][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = 0;
            }
        }

        for (int i = x1; i <= x2; i++) {
            a[y1][i] = 1;
            a[y2][i] = 1;
        }

        for (int i = y1 + 1; i < y2; i++) {
            a[i][x1] = 1;
            a[i][x2] = 1;
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == n - 1)
                    System.out.print(a[i][j]);
                else
                    System.out.print(a[i][j] + " ");
            }
            if (i < n - 1)
                System.out.println();
        }
    }
}
