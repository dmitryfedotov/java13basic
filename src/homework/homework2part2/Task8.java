package homework.homework2part2;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        System.out.println(countDigits(n));
    }

    static int countDigits(int n) {
        if (n < 10)
            return n;
        else {
            return n % 10 + countDigits((n - n % 10) / 10);
        }
    }
}
