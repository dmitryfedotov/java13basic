package homework.homework2part2;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        printDigits(n);
    }

    static void printDigits(int n) {
        if (n < 10)
            System.out.print(n + " ");
        else {
            int lastDigit = n % 10;
            printDigits((n - lastDigit) / 10);
            System.out.print(lastDigit + " ");
        }
    }
}
