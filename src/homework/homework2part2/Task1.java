package homework.homework2part2;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int[][] a = new int[n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = scanner.nextInt();
            }
        }

        int[]b = new int[n];
        for (int i = 0; i < n; i++) {
            b[i] = getMin(a[i]);
        }

        for (int i = 0; i < n; i++) {
            System.out.print(b[i] + " ");
        }
    }

    static int getMin(int[] a) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < a.length; i++) {
            if (a[i] < min)
                min = a[i];
        }
        return min;
    }
}
