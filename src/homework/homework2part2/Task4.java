package homework.homework2part2;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }

        int p = scanner.nextInt();

        int deletedRow = 0;
        int deletedColumn = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (a[i][j] == p) {
                    deletedRow = i;
                    deletedColumn = j;
                    break;
                }
            }
        }

        int[][] b = new int[n - 1][n - 1];
        int row = 0;
        int column = 0;
        for (int i = 0; i < n; i++) {
            if (i == deletedRow)
                continue;
            for (int j = 0; j < n; j++) {
                if (j == deletedColumn)
                    continue;
                b[row][column] = a[i][j];
                column++;
            }
            row++;
            column = 0;
        }

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1; j++) {
                if (j == n - 2)
                    System.out.print(b[i][j]);
                else
                    System.out.print(b[i][j] + " ");
            }
            if (i < n - 2)
                System.out.println();
        }
    }
}
