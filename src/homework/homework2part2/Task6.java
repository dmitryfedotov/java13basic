package homework.homework2part2;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int k = scanner.nextInt();

        int[][] nutrition = new int[7][4];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                nutrition[i][j] = scanner.nextInt();
            }
        }

        int sumA = 0;
        int sumB = 0;
        int sumC = 0;
        int sumK = 0;

        for (int i = 0; i < 7; i++) {
            sumA += nutrition[i][0];
            sumB += nutrition[i][1];
            sumC += nutrition[i][2];
            sumK += nutrition[i][3];
        }

        if (sumA <= a && sumB <= b && sumC <= c && sumK <= k)
            System.out.println("Отлично");
        else
            System.out.println("Нужно есть поменьше");
    }

}
