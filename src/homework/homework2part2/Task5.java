package homework.homework2part2;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] a = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }

        System.out.println(checkMatrix(a));
    }

    static boolean checkMatrix(int[][] a) {
        for (int i = 0; i < a.length / 2; i++) {
            for (int j = 0; j < a.length / 2; j++) {
                if (a[i][j] != a[a.length - 1 - j][a.length - 1 - i])
                    return false;
            }
        }

        return true;
    }
}
