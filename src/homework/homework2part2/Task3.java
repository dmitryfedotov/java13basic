package homework.homework2part2;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        String[][] a = new String[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = "0";
            }
        }

        a[y][x] = "K";
        setPoint(a, x + 2, y + 1);
        setPoint(a, x + 2, y - 1);
        setPoint(a, x + 1, y + 2);
        setPoint(a, x + 1, y - 2);
        setPoint(a, x - 2, y + 1);
        setPoint(a, x - 2, y - 1);
        setPoint(a, x - 1, y + 2);
        setPoint(a, x - 1, y - 2);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == n - 1)
                    System.out.print(a[i][j]);
                else
                    System.out.print(a[i][j] + " ");
            }
            if (i < n - 1)
                System.out.println();
        }
    }

    static void setPoint(String[][] a, int x, int y) {
        if (x >= 0 && y >= 0 && x < a.length && y < a.length)
            a[y][x] = "X";
    }
}
