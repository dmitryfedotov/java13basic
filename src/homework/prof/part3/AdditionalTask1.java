package homework.prof.part3;

import java.util.Stack;

// Additional task 1 - is a common case of add. task 2.
// So checkBracketSequence method solve both tasks.
public class AdditionalTask1 {
    public static void main(String[] args) {
        System.out.println(checkBracketSequence("(()()())"));
        System.out.println(checkBracketSequence(")("));
        System.out.println(checkBracketSequence("()("));
        System.out.println(checkBracketSequence("((()))"));

        System.out.println();
        System.out.println(checkBracketSequence("{()[]()}"));
        System.out.println(checkBracketSequence("{)(}"));
        System.out.println(checkBracketSequence("[}"));
        System.out.println(checkBracketSequence("[{(){}}][()]{}"));

    }

    public static boolean checkBracketSequence(String sequence) {
        Stack<Character> brackets = new Stack<>();
        for (int i = 0; i < sequence.length(); i++) {
            char currentBracket = sequence.charAt(i);
            if (currentBracket == '(' || currentBracket == '[' || currentBracket == '{')
                brackets.push(currentBracket);
            else {
                if (brackets.empty())
                    return false;

                char previousBracket = brackets.pop();
                if (!isPairBracket(previousBracket, currentBracket))
                    return false;
            }
        }

        return brackets.empty();
    }

    public static boolean isPairBracket(Character firstBracket, Character secondBracket) {
        return firstBracket == '(' && secondBracket == ')'
                || firstBracket == '[' && secondBracket == ']'
                || firstBracket == '{' && secondBracket == '}';
    }
}
