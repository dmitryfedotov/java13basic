package homework.prof.part3.task2;

import homework.prof.part3.task1.IsLike;

public class AnnotationTest {
    public static void main(String[] args) {
        checkAnnotation(ClassWithoutAnnotation.class);
        checkAnnotation(ClassWithAnnotation.class);
    }

    public static void checkAnnotation(Class<?> clazz) {
        IsLike annotation = clazz.getAnnotation(IsLike.class);
        if (annotation == null) {
            System.out.printf("Class %s haven't IsLike annotation\n", clazz);
            return;
        }

        System.out.printf("Class %s have IsLike annotation\n", clazz);
        System.out.printf("Value in annotation is %s\n", annotation.someBooleanValue());
    }
}
