package homework.prof.part3.task4;

import java.util.HashSet;
import java.util.Set;

public class Test {
    public static void main(String[] args) {
        System.out.println(getAllInterfaces(C.class));
    }

    public static Set<Class<?>> getAllInterfaces(Class<?> clazz) {
        Set<Class<?>> interfaces = new HashSet<>();

        // class C implements only InterfaceC
        readInterfacesRecursively(clazz, interfaces);

        // Its superclass B implements InterfaceB, which extends InterfaceA and InterfaceA1
        Class<?> superClass = clazz.getSuperclass();
        while (superClass != Object.class) {
            readInterfacesRecursively(superClass, interfaces);
            superClass = superClass.getSuperclass();
        }
        return interfaces;
    }

    public static void readInterfacesRecursively(Class<?> clazz, Set<Class<?>> interfaces) {
        for (Class<?> interfaceElement: clazz.getInterfaces()) {
            interfaces.add(interfaceElement);
            readInterfacesRecursively(interfaceElement, interfaces);
        }
    }
}
