package homework.prof.part3.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CallByReflection {
    public static void main(String[] args) {
        try {
            Class<?> clazz = Class.forName("homework.prof.part3.task3.APrinter");
            Method print = clazz.getMethod("print", int.class);
            APrinter printer = new APrinter();
            print.invoke(printer, 123);
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            System.out.println(e.getMessage());
        }
    }
}
