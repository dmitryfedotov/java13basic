package homework.prof.part2.task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Organisation {
    String name;

    public Organisation(String name) {
        this.name = name;
    }

    // This method must be static in Document class, but in task was given this definition.
    public Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> organizedDocuments = new HashMap<>();

        for (Document document: documents) {
            organizedDocuments.put(document.id, document);
        }

        return organizedDocuments;
    }

}
