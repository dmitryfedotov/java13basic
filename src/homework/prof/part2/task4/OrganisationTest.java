package homework.prof.part2.task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrganisationTest {
    public static void main(String[] args) {
        Organisation organisation = new Organisation("ООО Ололо");
        List<Document> documents = new ArrayList<>();
        documents.add(new Document(1, "doc 1", 10));
        documents.add(new Document(2, "doc 2", 1));
        documents.add(new Document(3, "new doc", 1));
        documents.add(new Document(4, "new new doc", 3));

        Map<Integer, Document> organizedDocuments = organisation.organizeDocuments(documents);
        System.out.println(organizedDocuments);
    }
}
