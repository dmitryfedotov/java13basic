package homework.prof.part2;

import java.util.*;
import java.util.function.BiFunction;

public class AdditionalTask {
    public static void main(String[] args) {
        String[] words = {"the","day","is","sunny","the","the","the",
                "sunny","is","is","day"};
        int k = 4;
        System.out.println(Arrays.toString(getTopWords(words, 4)));
    }

    public static String[] getTopWords(String[] words, int k) {
        Map<String, Integer> map = new HashMap<>();
        BiFunction<Integer, Integer, Integer> addiction = (int1, int2) -> int1 + int2;

        for (int i = 0; i < words.length; i++) {
            map.merge(words[i], 1, addiction);
//            int count = 0;
//            if (map.containsKey(words[i]))
//                count = map.get(words[i]);
//            map.put(words[i], ++count);
        }

        List<Map.Entry<String, Integer>> list = new ArrayList<>();
        list.addAll(map.entrySet());

//        Comparator<Map.Entry<String, Integer>> comparator = new Comparator<Map.Entry<String, Integer>>() {
//            @Override
//            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
//                int comparisonResult = Integer.compare(o2.getValue(), o1.getValue());
//                if (comparisonResult == 0)
//                    return o2.getKey().compareTo(o1.getKey());
//                else
//                    return comparisonResult;
//            }
//        };

        Comparator<Map.Entry<String, Integer>> comparator = (o1, o2) -> {
            int comparisonResult = Integer.compare(o2.getValue(), o1.getValue());
            if (comparisonResult == 0)
                return o2.getKey().compareTo(o1.getKey());
            else
                return comparisonResult;
        };
        list.sort(comparator);

        String[] topWords = new String[k];
        for (int i = 0; i < k; i++) {
            topWords[i] = list.get(i).getKey();
        }

        return topWords;
    }
}
