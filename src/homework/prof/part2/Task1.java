package homework.prof.part2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Task1 {
    public static void main(String[] args) {
        ArrayList<Integer> listOfInteger = new ArrayList<>();
        listOfInteger.add(1);
        listOfInteger.add(1);
        listOfInteger.add(2);
        listOfInteger.add(3);
        System.out.println(getUniqueElements(listOfInteger));
    }

    public static <T> Set<T> getUniqueElements(ArrayList<T> list) {
        Set<T> set = new HashSet<>(list);

        return set;
    }
}
