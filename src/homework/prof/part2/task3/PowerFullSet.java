package homework.prof.part2.task3;

import java.util.HashSet;
import java.util.Set;

// Logically, this class must be utility class,
// but in the task signatures of methods does not contain "static";
public class PowerFullSet {
    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> intersection = new HashSet<>();
        for (T element: set1) {
            if (set2.contains(element))
                intersection.add(element);
        }

        return intersection;
    }

    public <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> union = new HashSet<>();
        union.addAll(set1);
        union.addAll(set2);

        return union;
    }

    public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> complement = new HashSet<>();
        for (T element: set1) {
            if (!set2.contains(element))
                complement.add(element);
        }

        return complement;
    }
}
