package homework.prof.part2;

import java.util.*;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String t = scanner.nextLine();

        System.out.println(isAnagram(s, t));
    }

    public static boolean isAnagram(String s, String t) {
        List<Character> list = new ArrayList<>();
        for (Character c: s.toCharArray()) {
            list.add(c);
        }

        for (Character c: t.toCharArray()) {
            if (!list.remove(c))
                return false;
        }

        return list.size() == 0;
    }
}
