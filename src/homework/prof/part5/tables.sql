create table flowers (
    id serial primary key,
    name varchar(50) not null ,
    price float not null
);

 create table customers (
    id serial primary key,
    name varchar(50) not null ,
    phone varchar(30) not null
);

create table orders (
    id serial primary key,
    date timestamp not null,
    flower_id int references flowers (id) not null ,
    customer_id int references customers (id) not null ,
    quantity int not null check ( quantity > 0 and quantity <= 1000 )
);
commit;