--1
select o.date, f.name, o.quantity, c.name, c.phone
from orders o
    inner join customers c on c.id = o.customer_id
    inner join flowers f on o.flower_id = f.id
where o.id = 7
order by o.date;

--2
select o.date, f.name, o.quantity
from orders o
    inner join flowers f on f.id = o.flower_id
where o.customer_id = 1
    and o.date >= now() - interval '1month';

--3
select f.name, o.quantity
from orders o
    inner join flowers f on f.id = o.flower_id
where quantity = (select max(quantity)
                    from orders);

--4
select sum(o.quantity * f.price)
from orders o inner join flowers f on f.id = o.flower_id;