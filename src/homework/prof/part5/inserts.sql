insert into flowers (name, price)
values ('Роза', 10);

insert into flowers (name, price)
values ('Лилия', 50);

insert into flowers (name, price)
values ('Роза', 25);

insert into customers (name, phone)
values ('Вася', '8-800-2000-600');

insert into customers (name, phone)
values ('Коля', '8-800-555-35-35');

insert into customers (name, phone)
values ('Петя', '8(123)456-78-90');

insert into orders (date, flower_id, customer_id, quantity)
values ('2022-09-01 12:12:12.123456', 1, 1, 1000);

insert into orders (date, flower_id, customer_id, quantity)
values ('2022-10-13', 2, 1, 5);

insert into orders (date, flower_id, customer_id, quantity)
values ('2022-10-20', 1, 2, 500);

insert into orders (date, flower_id, customer_id, quantity)
values ('2022-11-30', 1, 2, 10);

insert into orders (date, flower_id, customer_id, quantity)
values ('2022-12-15', 3, 3, 55);

insert into orders (date, flower_id, customer_id, quantity)
values ('2022-12-31', 1, 1, 10);

insert into orders (date, flower_id, customer_id, quantity)
values ('2022-12-31', 1, 2, 20);

insert into orders (date, flower_id, customer_id, quantity)
values ('2022-12-31', 1, 3, 50);

insert into orders (date, flower_id, customer_id, quantity)
values ('2022-12-31', 2, 1, 99);

insert into orders (date, flower_id, customer_id, quantity)
values ('2023-01-11', 3, 1, 499);

insert into orders (date, flower_id, customer_id, quantity)
values ('2023-01-15', 3, 2, 7);

insert into orders (date, flower_id, customer_id, quantity)
values ('2023-01-20', 2, 2, 17);

commit;