package homework.prof.part4;

import java.util.List;

public class Task5 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");
        printList(list);
    }

    public static void printList(List<String> list) {
        StringBuilder builder = new StringBuilder();
        list.stream()
                .map(String::toUpperCase)
                .forEach(s -> {
                    if (!builder.isEmpty())
                        builder.append(", ");
                    builder.append(s);
                });

        System.out.println(builder.toString());
    }
}
