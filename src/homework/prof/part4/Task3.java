package homework.prof.part4;

import java.util.List;

public class Task3 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "", "", "def", "qqq");
        System.out.println(getCountOfNonEmptyStrings(list));
    }

    public static long getCountOfNonEmptyStrings(List<String> list) {
        return list.stream()
                .filter(s -> !s.equals(""))
                .count();
    }
}
