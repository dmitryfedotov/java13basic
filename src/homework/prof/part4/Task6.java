package homework.prof.part4;

import java.util.HashSet;
import java.util.Set;

public class Task6 {
    public static void main(String[] args) {
        Set<Set<Integer>> setOfSets = new HashSet<>();
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);
        set1.add(4);
        set1.add(5);

        Set<Integer> set2 = new HashSet<>();
        set2.add(5);
        set2.add(6);
        set2.add(7);
        set2.add(8);
        set2.add(9);
        setOfSets.add(set1);
        setOfSets.add(set2);

        System.out.println(extractSets(setOfSets));
    }

    public static Set<Integer> extractSets(Set<Set<Integer>> setOfSets) {
        return setOfSets.stream().reduce(new HashSet<>(), (resultSet, innerSet) -> {
            resultSet.addAll(innerSet);
            return resultSet;
        });

    }
}
