package homework.prof.part4;

import java.util.stream.IntStream;

public class Task1 {
    public static void main(String[] args) {
        int sum = IntStream.range(1, 101)
                .filter(x -> x % 2 == 0)
                .reduce(0, (x, y) -> x + y);
        System.out.println(sum);
    }
}
