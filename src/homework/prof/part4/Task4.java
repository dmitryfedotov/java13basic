package homework.prof.part4;

import java.util.List;

public class Task4 {
    public static void main(String[] args) {
        List<Double> list = List.of(1.0, 12.1, -9.9, -12.0, 4.4);
        System.out.println(getSortedList(list));
    }

    public static List<Double> getSortedList(List<Double> list) {
        return list.stream()
                .sorted(Double::compare)
                .toList();
    }
}
