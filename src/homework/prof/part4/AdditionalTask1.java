package homework.prof.part4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AdditionalTask1 {
    public static void main(String[] args) {
        System.out.println(compareWordsApproximately("cat", "cats"));
        System.out.println(compareWordsApproximately("cat", "cut"));
        System.out.println(compareWordsApproximately("cat", "nut"));
    }

    public static boolean compareWordsApproximately(String word1, String word2) {
        List<Character> list1 = getListFromString(word1);
        List<Character> list2 = getListFromString(word2);

        retainLists(list1, list2);

        return list1.size() <= 1 && list2.size() <= 1;
    }

    static List<Character> getListFromString(String s) {
        List<Character> list = new ArrayList<>();
        for (char c: s.toCharArray()) {
            list.add(c);
        }
        return list;
    }

    static void retainLists(List<Character> list1, List<Character> list2) {
        Iterator<Character> iterator = list1.iterator();
        while (iterator.hasNext()) {
            Character c = iterator.next();
            if (list2.contains(c)) {
                iterator.remove();
                list2.remove(c);
            }
        }
    }
}
