package homework.prof.part4;

import java.util.List;

public class Task2 {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4, 5);
        System.out.println(multiply(list));
    }

    public static int multiply(List<Integer> list) {
        return list.stream()
                .reduce(1, (x, y) -> x * y);
    }
}
