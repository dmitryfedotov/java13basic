package homework.prof.part1.task6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;

public class FormValidator {
    private FormValidator() { };

    public static void checkName(String str) {
        if (!str.matches("[А-ЯЁA-Z][а-яёa-z]{1,19}"))
            throw new IllegalArgumentException("Name must start with capital letter and be from 2 to 20 characters long");
    }

    public static void checkBirthdate(String str) {

        //SimpleDateFormat formatter = new SimpleDateFormat("dd.mm.yyyy");
        //try {
        //    Date date = formatter.parse(str);
//        } catch (ParseException e) {
//            throw new IllegalArgumentException("Date must be in DD.MM.YYYY format");
//        }

        //if (date.before(new Date(1900, 1, 1)))
        int day = Integer.parseInt(str.substring(0, 2));
        int month = Integer.parseInt(str.substring(3, 5));
        int year = Integer.parseInt(str.substring(6));

        if (year < 1900)
            throw new IllegalArgumentException("Year must be greater than 1900");

        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setLenient(false);
        calendar.set(GregorianCalendar.YEAR, year);
        calendar.set(GregorianCalendar.MONTH, month - 1);
        calendar.set(GregorianCalendar.DAY_OF_MONTH, day);

        if (calendar.getTime().after(new Date()))
            throw new IllegalArgumentException("Date can't be greater than current date");
    }

    public static void checkGender(String str) {
        Gender.getByName(str);
    }

    public static void checkHeight(String str) {
        double height = Double.parseDouble(str);
        if (height < 0)
            throw new IllegalArgumentException("Height must be greater than 0");
    }

}
