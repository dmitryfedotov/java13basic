package homework.prof.part1.task6;

public class FormValidatorTest {
    public static void main(String[] args) {
        System.out.println("Name:");
        FormValidator.checkName("Oleg");

        System.out.println("First exception:");
        try {
            FormValidator.checkName("Oleg1");
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }

        System.out.println("Second exception:");
        try {
            FormValidator.checkName("OlegG");
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }

        System.out.println("Birthdate:");
        FormValidator.checkBirthdate("12.12.2000");
        FormValidator.checkBirthdate("12.01.2000");

        System.out.println("First exception:");
        try {
            FormValidator.checkBirthdate("12.13.2000");
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }

        System.out.println("Second exception:");
        try {
            FormValidator.checkBirthdate("12.10.1800");
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }

        System.out.println("Third exception:");
        try {
            FormValidator.checkBirthdate("99.10.2000");
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }

        System.out.println("Gender:");
        FormValidator.checkGender("male");
        FormValidator.checkGender("female");
        FormValidator.checkGender("feMale");
        System.out.println("First exception:");
        try {
            FormValidator.checkGender("battle helicopter");
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }

        System.out.println("Height:");
        FormValidator.checkHeight("100");
        FormValidator.checkHeight("100.123");
        System.out.println("First exception:");
        try {
            FormValidator.checkHeight("-1");
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }

        System.out.println("Second exception:");
        try {
            FormValidator.checkHeight("Высокий");
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }
    }
}
