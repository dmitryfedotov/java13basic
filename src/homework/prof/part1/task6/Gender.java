package homework.prof.part1.task6;

public enum Gender {
    MALE("male"),
    FEMALE("female");

    private String name;
    Gender(String name) {
        this.name = name;
    }

    public static Gender getByName(String name) {
        for (Gender gender: Gender.values()) {
            if (gender.name.equalsIgnoreCase(name))
                return gender;
        }

        throw new IllegalArgumentException("Unknown gender name");
    }
}
