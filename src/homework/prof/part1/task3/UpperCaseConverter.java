package homework.prof.part1.task3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;
import java.util.regex.Pattern;

public class UpperCaseConverter {
    public static final String FOLDER_PATH = "C:/Users/Дмитрий/IdeaProjects/java13basic/src/homework/prof/part1/task3/";
    public static final String INPUT_FILENAME = "input.txt";
    public static final String OUTPUT_FILENAME = "output.txt";
    private UpperCaseConverter() {};

    public static void convert() throws IOException {
        try (
                Scanner scanner = new Scanner(new File(FOLDER_PATH + INPUT_FILENAME));
                Writer writer = new FileWriter(FOLDER_PATH + OUTPUT_FILENAME)
        ) {
            Pattern lowerCasePattern = Pattern.compile("[a-z]");
            while (scanner.hasNext()) {
                String replaced = lowerCasePattern.matcher(scanner.nextLine()).replaceAll(
                        match -> match.group(0).toUpperCase());
                writer.write(replaced + "\n");
            }
        }
    }
}
