package homework.prof.part1;

import java.util.Arrays;
import java.util.Scanner;

public class AdditionalTask2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }

        int number = scanner.nextInt();
        // I'm going to make my own search instead of Arrays.binarySearch().
        int begin = 0;
        int end = n - 1;
        int index = -1;
        while (begin < end) {
            int middle = begin + (end - begin) / 2;
            if (array[middle] == number) {
                index = middle;
                break;
            } else if (array[middle] > number) {
                end = middle - 1;
            } else {
                begin = middle + 1;
            }
        }
        /* тут будет по-русски, т.к. по английски я такое объяснить не смогу.
        * рассмотрим n = 2^k.
        * В худшем случае мы будем делить интервал поиска пополам до тех пор,
        * пока концы интервала не схлопнутся.
        * Таким образом наш интервал будет постоянно уменьшаться вдвое в каждой итерации.
        * Такой будет длина интервала на каждой итерации:
        * 1. n
        * 2. n / 2
        * 3. n / 2^2
        * 4. n / 2^3
        * ...
        * k. n / 2^k
        * Значит, на итерации k наш интервал поиска схлопнется.
        * Найдем k.
        * n / 2^k = 1;
        * 2^k = n;
        * k = log2(n); */
        System.out.println(index);
    }

}
