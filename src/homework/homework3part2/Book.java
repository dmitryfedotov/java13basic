package homework.homework3part2;

import java.util.ArrayList;

public class Book {
    private String title;
    private String author;
    private ArrayList<Integer> reviews;

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
        this.reviews = new ArrayList<>();
    }

    public void addReview(Integer review) {
        reviews.add(review);
    }

    public double averageReview() {
        if (reviews.size() == 0)
            return 0;

        double sum = 0;
        for (Integer review: reviews) {
            sum += review;
        }

        return sum / reviews.size();
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return String.format("\"%s\" by %s", title, author);
    }
}
