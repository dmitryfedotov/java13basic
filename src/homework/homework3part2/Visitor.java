package homework.homework3part2;

import java.util.UUID;

public class Visitor {
    private String name;
    private UUID id;

    public Visitor(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public UUID getId() {
        return id;
    }
}
