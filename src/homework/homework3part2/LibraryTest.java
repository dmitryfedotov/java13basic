package homework.homework3part2;

import java.util.ArrayList;

public class LibraryTest {
    public static void main(String[] args) {
        Library library = new Library();
        // task 1. Create books
        System.out.println("Task 1");
        library.addBook("My first book", "Ivanov");
        library.addBook("My second book", "Ivanov");
        library.addBook("My third book", "Ivanov");

        library.addBook("The first", "Petrov");
        library.addBook("Very second", "Petrov");
        library.addBook("My second", "Petrov");
        library.addBook("My third", "Petrov");

        try {
            library.addBook("My third", "Ivanov");
        } catch (Exception e) {
            System.out.println(e);
        }

        // task 3. Get book by name.
        System.out.println();
        System.out.println("Task 3");
        Book book = library.getBookByTitle("The first");
        System.out.println(book);

        // task 4. Get books by author.
        System.out.println();
        System.out.println("Task 4");
        ArrayList<Book> books = library.getBooksByAuthor("Petrov");
        System.out.println(books);

        Visitor oleg = new Visitor("Oleg");
        Visitor pavel = new Visitor("Pavel");
        Visitor denis = new Visitor("Denis");

        // task 5. Borrow books.
        System.out.println();
        System.out.println("Task 5");
        System.out.println(oleg.getId());
        library.borrowBookByTitle("The first", oleg);
        System.out.println(oleg.getId());
        library.borrowBookByTitle("My third book", pavel);

        // task 5.a. The book exists.
        System.out.println();
        System.out.println("Task 5.a");
        try {
            library.borrowBookByTitle("Some book", denis);
        } catch (Exception e) {
            System.out.println(e);
        }

        // task 5.b. Visitor haven't another book.
        System.out.println();
        System.out.println("Task 5.b");
        try {
            library.borrowBookByTitle("Very second", oleg);
        } catch (Exception e) {
            System.out.println(e);
        }

        // task 5.c. The book is not borrowed.
        System.out.println();
        System.out.println("Task 5.c");
        try {
            library.borrowBookByTitle("The first", denis);
        } catch (Exception e) {
            System.out.println(e);
        }

        // task 6. Return book.
        System.out.println();
        System.out.println("Task 6");
        library.returnBookByTitle("The first", oleg);

        // task 7. Do not return book, borrowed by another visitor.
        System.out.println();
        System.out.println("Task 7");
        try {
            library.returnBookByTitle("My third book", oleg);
        } catch (Exception e) {
            System.out.println(e);
        }

        // task 2. Delete book.
        System.out.println();
        System.out.println("Task 2");
        library.deleteBookByTitle("My second");
        // do not delete borrowed books
        try {
            library.deleteBookByTitle("My third book");
        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println();
        System.out.println("Task 8");
        library.borrowBookByTitle("The first", denis);
        library.returnBookByTitle("The first", denis, 5);
        library.borrowBookByTitle("The first", oleg);
        library.returnBookByTitle("The first", oleg, 4);
        System.out.println(library.getAverageReviewByTitle("The first"));
    }
}
