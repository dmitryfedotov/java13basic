package homework.homework3part2;

public class BorrowedBook {
    private Book book;
    private Visitor visitor;

    public BorrowedBook(Book book, Visitor visitor) {
        this.book = book;
        this.visitor = visitor;
    }

    public Book getBook() {
        return book;
    }

    public Visitor getVisitor() {
        return visitor;
    }
}
