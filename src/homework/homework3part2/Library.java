package homework.homework3part2;

import java.util.ArrayList;
import java.util.UUID;

public class Library {
    private ArrayList<Book> books;
    private ArrayList<BorrowedBook> borrowedBooks;

    public Library() {
        books = new ArrayList<>();
        borrowedBooks = new ArrayList<>();
    }

    public void addBook(String title, String author) {
        for (Book book : books) {
            if (book.getTitle().equals(title)) {
                String exceptionMessage = String.format("Book \"%s\" is already in library.", title);
                throw new IllegalArgumentException(exceptionMessage);
            }
        }

        books.add(new Book(title, author));
    }

    public void deleteBookByTitle(String title) {
        Book book = getBookByTitle(title);

        if (book == null) {
            String exceptionMessage = String.format("Book \"%s\" was not found in library.", title);
            throw new IllegalArgumentException(exceptionMessage);
        }

        for (BorrowedBook borrowedBook : borrowedBooks) {
            if (borrowedBook.getBook() == book) {
                String exceptionMessage = String.format("Book \"%s\" was borrowed.", title);
                throw new IllegalArgumentException(exceptionMessage);
            }
        }

        // if book exist and not borrowed it can be deleted.
        books.remove(book);
    }

    public Book getBookByTitle(String title) {
        Book book = null;
        for (Book value : books) {
            if (value.getTitle().equals(title)) {
                book = value;
                break;
            }
        }

        return book;
    }

    public ArrayList<Book> getBooksByAuthor(String author) {
        ArrayList<Book> list = new ArrayList<>();

        for (Book book: books) {
            if (book.getAuthor().equals(author))
                list.add(book);
        }

        return list;
    }

    public void borrowBookByTitle(String title, Visitor visitor) {
        Book book = getBookByTitle(title);

        if (book == null) {
            String exceptionMessage = String.format("Book \"%s\" was not found in library.", title);
            throw new IllegalArgumentException(exceptionMessage);
        }

        for (BorrowedBook borrowedBook: borrowedBooks) {
            if (borrowedBook.getBook() == book) {
                String exceptionMessage = String.format("Book \"%s\" was borrowed by another visitor.", title);
                throw new IllegalArgumentException(exceptionMessage);
            } else if (borrowedBook.getVisitor() == visitor) {
                String exceptionMessage = String.format("Visitor \"%s\" borrowed another book.", visitor.getName());
                throw new IllegalArgumentException(exceptionMessage);
            }
        }

        if (visitor.getId() == null)
            visitor.setId(UUID.randomUUID());
        borrowedBooks.add(new BorrowedBook(book, visitor));
    }

    public void returnBookByTitle(String title, Visitor visitor) {
        returnBookByTitle(title, visitor, null);
    }
    public void returnBookByTitle(String title, Visitor visitor, Integer review) {
        if (review != null && (review < 1 || review > 5)) {
            String exceptionMessage = String.format("Review must be between 1 and 5.", title);
            throw new IllegalArgumentException(exceptionMessage);
        }

        Book book = getBookByTitle(title);
        if (book == null) {
            String exceptionMessage = String.format("Book \"%s\" was not found in library.", title);
            throw new IllegalArgumentException(exceptionMessage);
        }

        BorrowedBook foundBorrowedBook = null;
        for (BorrowedBook borrowedBook: borrowedBooks) {
            if (borrowedBook.getBook() == book) {
                foundBorrowedBook = borrowedBook;
                break;
            }
        }

        if (foundBorrowedBook == null) {
            String exceptionMessage = String.format("Book \"%s\" is not borrowed.", title);
            throw new IllegalArgumentException(exceptionMessage);
        } else if (foundBorrowedBook.getVisitor() != visitor) {
            String exceptionMessage = String.format("Book \"%s\" was borrowed by another visitor.", title);
            throw new IllegalArgumentException(exceptionMessage);
        } else {
            borrowedBooks.remove(foundBorrowedBook);
        }

        if (review != null) {
            book.addReview(review);
        }
    }

    public double getAverageReviewByTitle(String title) {
        Book book = getBookByTitle(title);
        if (book == null) {
            String exceptionMessage = String.format("Book \"%s\" was not found in library.", title);
            throw new IllegalArgumentException(exceptionMessage);
        }

        return book.averageReview();
    }
}