package homework.homework2part1;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String s = scanner.next();
        String[] alphabet = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--",
                "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--",
                "-.--", "-..-", "..-..", "..--", ".-.-"};
        for (int i = 0; i < s.length(); i++) {
            int index = (int) s.charAt(i) - (int)'А';
            System.out.print(alphabet[index] + " ");
        }
    }
}
