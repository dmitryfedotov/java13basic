package homework.homework2part1;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];

        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }

        int m = scanner.nextInt();
        int index = 0;
        for (int i = 0; i < n; i++) {
            if (a[i] > m)
                break;
            else
                index++;
        }

        System.out.println(index);
    }
}
