package homework.homework2part1;

import java.util.Scanner;

public class AdditionalTask1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        while (n < 8) {
            System.out.printf("Пароль с %s количеством символов небезопасен", n);
            System.out.println();
            n = scanner.nextInt();
        }

        String upperCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
        String specialSymbols = "_*-";

        String password = "";

        // признак использования того или иного набора символов.
        boolean upperCaseLettersUsed = false;
        boolean lowerCaseLettersUsed = false;
        boolean numbersUsed = false;
        boolean specialSymbolsUsed = false;

        // количество неиспользованных наборов символов.
        int unusedSets = 4;

        for (int i = 0; i < n; i++) {
            String setForPassword = "";
            // в пароле должны присутсовать символы из всех наборов.
            // значит, мы можем использовать какой-либо набор только в том случае,
            // если для неиспользованных наборов в пароле еще есть место.
            boolean setsCanBeReused = unusedSets < n - i;
            if (!upperCaseLettersUsed || setsCanBeReused)
                setForPassword += upperCaseLetters;
            if (!lowerCaseLettersUsed || setsCanBeReused)
                setForPassword += lowerCaseLetters;
            if (!numbersUsed || setsCanBeReused)
                setForPassword += numbers;
            if (!specialSymbolsUsed || setsCanBeReused)
                setForPassword += specialSymbols;

            int position = (int) (Math.random() * setForPassword.length());
            String chosenSymbol = "" + setForPassword.charAt(position);
            password += chosenSymbol;

            // теперь смотрим, из какого набора выбран символ.
            // если этот набор не помечен, как использованный, значит, делаем это
            // и уменьшаем количество неиспользованных наборов
            if (!upperCaseLettersUsed && upperCaseLetters.contains(chosenSymbol)) {
                upperCaseLettersUsed = true;
                unusedSets--;
            } else if (!lowerCaseLettersUsed && lowerCaseLetters.contains(chosenSymbol)) {
                lowerCaseLettersUsed = true;
                unusedSets--;
            } else if (!numbersUsed && numbers.contains(chosenSymbol)) {
                numbersUsed = true;
                unusedSets--;
            } else if (!specialSymbolsUsed && specialSymbols.contains(chosenSymbol)) {
                specialSymbolsUsed = true;
                unusedSets--;
            }
        }

        System.out.println(password);
    }
}
