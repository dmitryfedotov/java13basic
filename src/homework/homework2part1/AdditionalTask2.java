package homework.homework2part1;

import java.util.Scanner;

public class AdditionalTask2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] a = new int[n];

        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }

        int startIndex = 0;
        int endIndex = n - 1;

        int[] b = new int[n];
        for (int i = 0; i < n; i++) {
            if (Math.abs(a[startIndex]) > Math.abs(a[endIndex])) {
                b[n - i - 1] = (int) Math.pow(a[startIndex], 2);
                startIndex++;
            } else {
                b[n - i - 1] = (int) Math.pow(a[endIndex], 2);
                endIndex--;
            }
        }

        for (int i = 0; i < n; i++) {
            System.out.print(b[i] + " ");
        }

    }
}
