package homework.homework2part1;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        playWithMe();
    }

    static void playWithMe() {
        int n = (int) (Math.random() * 1001);

        Scanner scanner = new Scanner(System.in);
        int currentAnswer;
        while ((currentAnswer = scanner.nextInt()) >= 0) {
            if (currentAnswer == n) {
                System.out.println("Победа!");
                break;
            } else if (currentAnswer < n) {
                System.out.println("Это число меньше загаданного.");
            } else {
                System.out.println("Это число больше загаданного.");
            }
        }
    }
}
