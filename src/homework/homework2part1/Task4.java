package homework.homework2part1;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }

        int current = a[0];
        int count = 1;
        for (int i = 1; i < n; i++) {
            if (a[i] == current)
                count++;
            else {
                System.out.println(count + " " + current);
                current = a[i];
                count = 1;
            }
        }
        System.out.println(count + " " + current);
    }
}
