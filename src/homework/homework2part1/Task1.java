package homework.homework2part1;

import java.util.Locale;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.ENGLISH);

        int n = scanner.nextInt();
        double[] a = new double[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextDouble();
        }

        System.out.println(getAverage(a));
    }

    static double getAverage(double[] a) {
        double sum = 0;
        for (double i: a) {
            sum += i;
        }

        return sum / a.length;
    }
}
