package homework.homework2part1;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        String[] s = new String[n];

        for (int i = 0; i < n; i++) {
            s[i] = scanner.next();
        }

        String regex = s[0];
        for (int i = 1; i < n; i++) {
            if (s[i].matches(regex)) {
                System.out.println(s[i]);
                break;
            } else {
                regex = regex + "|" + s[i];
            }
        }
    }
}
