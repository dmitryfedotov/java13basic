package homework.homework2part1;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }

        int m = scanner.nextInt();
        int delta = Integer.MAX_VALUE;
        int closest = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            int currentDelta = Math.abs(a[i] - m);
            if (currentDelta <= delta) {
                delta = currentDelta;
                if (a[i] > closest)
                    closest = a[i];
            }
        }
        System.out.println(closest);
    }
}
