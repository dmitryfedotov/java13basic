package homework.homework3part1;

public class TriangleChecker {
    public static boolean triangleCanBeCreated(double a, double b, double c) {
        if (a <= 0 || b <= 0 || c <= 0)
            return false;

        double a2 = a * a;
        double b2 = b * b;
        double c2 = c * c;
        return a2 + b2 >= c2
                && a2 + c2 >= b2
                && b2 + c2 >= a2;
    }
}
