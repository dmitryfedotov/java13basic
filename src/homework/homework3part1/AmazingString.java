package homework.homework3part1;

public class AmazingString {
    private char[] value;

    public AmazingString(char[] value) {
        this.value = value;
    }

    public AmazingString(String value) {
        this.value = stringToCharArray(value);
    }

    public char charAt(int index) {
        return value[index];
    }

    public int length() {
        return value.length;
    }

    public void print() {
        for (int i = 0; i < value.length; i++) {
            System.out.print(value[i]);
        }
        System.out.println();
    }

    public boolean haveSubstring(char[] substring) {

        if (substring.length == 0) {
            return true;
        }

        for (int i = 0; i < value.length - substring.length; i++) {
            if (value[i] == substring[0]) {
                boolean substringIncluded = true;
                for (int j = 1; j < substring.length; j++) {
                    if (value[i + j] != substring[j]) {
                        substringIncluded = false;
                        break;
                    }
                }
                if (substringIncluded)
                    return true;
            }
        }

        return false;
    }

    public boolean haveSubstring(String substring) {
        return haveSubstring(stringToCharArray(substring));
    }

    public void deleteLeadingSpaces() {
        int spaceAmount = 0;

        for (int i = 0; i < value.length && value[i] == ' '; i++) {
            spaceAmount++;
        }

        if (spaceAmount == 0)
            return;
        char[] newValue = new char[value.length - spaceAmount];
        System.arraycopy(value, spaceAmount, newValue, 0, newValue.length);
        value = newValue;
    }

    public void reverse() {
        for (int i = 0; i < value.length / 2; i++) {
            char tmp = value[i];
            value[i] = value[value.length - 1 - i];
            value[value.length - 1 - i] = tmp;
        }
    }

    private char[] stringToCharArray(String s) {
        char[] array = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            array[i] = s.charAt(i);
        }
        return array;
    }

}
