package homework.homework3part1;

public class AtmTest {
    public static void main(String[] args) {
        Atm atm1 = new Atm(75, CourseType.RUB_TO_USD);
        Atm atm2 = new Atm(0.015, CourseType.USD_TO_RUB);
        Atm favouriteAtm = new Atm(30, CourseType.RUB_TO_USD);

        System.out.println(Atm.getAtmAmount());

        System.out.println(favouriteAtm.convertRoublesToDollars(3000));
        System.out.println(atm2.convertDollarsToRoubles(1000));
    }
}
