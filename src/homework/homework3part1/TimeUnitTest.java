package homework.homework3part1;

public class TimeUnitTest {
    public static void main(String[] args) {
        TimeUnit timeUnit1 = new TimeUnit(1, 3, 10);
        timeUnit1.print24HourClock();
        timeUnit1.print12HourClock();

        TimeUnit timeUnit2 = new TimeUnit(12, 3, 10);
        timeUnit2.print24HourClock();
        timeUnit2.print12HourClock();

        try {
            TimeUnit timeUnit3 = new TimeUnit(25, 3, 10);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
