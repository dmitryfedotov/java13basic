package homework.homework3part1;

public class Cat {
    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");
    }

    private void eat() {
        System.out.println("Eat");
    }

    public void status() {
        switch ((int) (Math.random() * 3)) {
            case 0 -> sleep();
            case 1 -> meow();
            case 2 -> eat();
        }
    }
}
