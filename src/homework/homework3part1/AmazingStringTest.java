package homework.homework3part1;

public class AmazingStringTest {
    public static void main(String[] args) {
        char[] charArray = {'s', 't', 'r', 'i', 'n', 'g'};
        AmazingString stringFromArray = new AmazingString(charArray);
        AmazingString stringFromString = new AmazingString("another string");

        System.out.println(stringFromArray.charAt(3));
        System.out.println(stringFromString.length());
        stringFromArray.print();
        System.out.println(stringFromArray.haveSubstring("the"));

        char[] charArrayThe = {'t', 'h', 'e'};
        System.out.println(stringFromString.haveSubstring(charArrayThe));

        AmazingString stringWithSpaces = new AmazingString("   symbols and space");
        stringWithSpaces.print();
        stringWithSpaces.deleteLeadingSpaces();
        stringWithSpaces.print();

        stringWithSpaces.reverse();
        stringWithSpaces.print();

    }
}
