package homework.homework3part1;

public class TriangleCheckerTest {
    public static void main(String[] args) {
        System.out.println(TriangleChecker.triangleCanBeCreated(1, 2, 3));
        System.out.println(TriangleChecker.triangleCanBeCreated(2, 2, -3));
        System.out.println(TriangleChecker.triangleCanBeCreated(3, 4, 5));
        System.out.println(TriangleChecker.triangleCanBeCreated(9, 4, 5));
    }
}
