package homework.homework3part1;

public class TimeUnit {
    final static int HOURS_IN_DAY = 24;
    final static int MINUTES_IN_HOUR = 60;
    final static int SECONDS_IN_MINUTE = 60;
    private int hours;
    private int minutes;
    private int seconds;

    public TimeUnit(int hours, int minutes, int seconds) {
        if (hours < 0 || hours > HOURS_IN_DAY) {
            throw new IllegalArgumentException("Hours must be between 0 and " + HOURS_IN_DAY);
        }
        if (minutes < 0 || minutes > MINUTES_IN_HOUR) {
            throw new IllegalArgumentException("Minutes must be between 0 and " + MINUTES_IN_HOUR);
        }
        if (seconds < 0 || seconds > seconds) {
            throw new IllegalArgumentException("Seconds must be between 0 and " + SECONDS_IN_MINUTE);
        }

        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public TimeUnit(int hours, int minutes) {
        this(hours, minutes, 0);
    }

    public TimeUnit(int hours) {
        this(hours, 0);
    }

    public void print24HourClock() {

        System.out.printf("%02d:%02d:%02d", hours, minutes, seconds);
        System.out.println();
    }

    public void print12HourClock() {
        int convertedHours = hours;
        String period;
        if (hours >= 12) {
            period = "pm";
            if (hours > 12)
                convertedHours -= 12;
        } else {
            period = "am";
        }

        System.out.printf("%02d:%02d:%02d %s", convertedHours, minutes, seconds, period);
        System.out.println();
    }
}
