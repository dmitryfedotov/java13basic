package homework.homework3part1;

public class Atm {
    private double course;
    private CourseType courseType;

    public static int getAtmAmount() {
        return atmAmount;
    }

    private static int atmAmount;

    public Atm(double course, CourseType courseType) {
        if (course <= 0)
            throw new IllegalArgumentException("Curse must be positive");

        this.course = course;
        this.courseType = courseType;

        atmAmount++;
    }

    public double convertRoublesToDollars(double roubles) {
        if (courseType == CourseType.RUB_TO_USD)
            return roubles / course;
        else
            return roubles * course;
    }

    public double convertDollarsToRoubles(double dollars) {
        if (courseType == CourseType.RUB_TO_USD)
            return dollars * course;
        else
            return dollars / course;
    }
}
