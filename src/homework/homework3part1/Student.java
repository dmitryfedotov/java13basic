package homework.homework3part1;

public class Student {
    @Override
    public String toString() {
        return surname + " " + name;
    }

    private String name;

    private String surname;
    private int[] grades;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        // make sure, that grades will have 10 or fewer elements.
        int startPosition = 0;
        if (grades.length > 10) {
            startPosition = grades.length - 10;
            this.grades = new int[10];
        } else {
            this.grades = new int[grades.length];
        }

        for (int i = startPosition; i < grades.length; i++) {
            this.grades[i - startPosition] = grades[i];
        }
    }

    public void addGrade(int grade) {
        /*
        Самая первая оценка должна быть удалена,
        новая должна сохраниться в конце массива (т.е. массив должен сдвинуться на 1 влево).
         */
        int i;
        for (i = 0; i < grades.length - 1; i++) {
            grades[i] = grades[i + 1];
        }
        grades[i] = grade;
    }

    public double getAverageGrade() {
        if (grades.length == 0)
            return 0;

        double sum = 0;
        for (int i = 0; i < grades.length; i++) {
            sum += grades[i];
        }

        return sum / grades.length;
    }

}
