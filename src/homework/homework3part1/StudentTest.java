package homework.homework3part1;

import java.util.Arrays;

public class StudentTest {
    public static void main(String[] args) {
        Student student1 = new Student();
        student1.setName("Ivan");
        student1.setSurname("Ivanov");
        int[] grades1 = {5, 5, 4, 4, 3, 4, 5, 3, 2, 4};
        student1.setGrades(grades1);
        System.out.print("Grades before: ");
        System.out.println(Arrays.toString(student1.getGrades()));
        System.out.print("Average grade before: ");
        System.out.println(student1.getAverageGrade());

        student1.addGrade(2);
        System.out.print("Grades after: ");
        System.out.println(Arrays.toString(student1.getGrades()));
        System.out.print("Average grade after: ");
        System.out.println(student1.getAverageGrade());

        Student student2 = new Student();
        student2.setName("Petr");
        student2.setSurname("Petrov");
        int[] grades2 = {2, 2, 2, 2, 4};
        student2.setGrades(grades2);
        System.out.print("Grades before: ");
        System.out.println(Arrays.toString(student2.getGrades()));
        System.out.print("Average grade before: ");
        System.out.println(student2.getAverageGrade());

        student2.addGrade(5);
        System.out.print("Grades after: ");
        System.out.println(Arrays.toString(student2.getGrades()));
        System.out.print("Average grade after: ");
        System.out.println(student2.getAverageGrade());

        Student student3 = new Student();
        student3.setName("Sidor");
        student3.setSurname("Bobrov");
        int[] grades3 = {2, 2, 2, 2, 4, 5, 5, 5, 5, 5};
        student3.setGrades(grades3);

        Student[] students = {student1, student2, student3};
        Student bestStudent = StudentService.bestStudent(students);
        System.out.printf("Best student is %s %s with average grade %s",
                bestStudent.getSurname(), bestStudent.getName(), bestStudent.getAverageGrade());
        System.out.println();

        System.out.println("Students before sorting:");
        System.out.println(Arrays.toString(students));
        StudentService.sortBySurname(students);
        System.out.println("Students after sorting:");
        System.out.println(Arrays.toString(students));
    }
}
