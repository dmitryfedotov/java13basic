package homework.homework3part1;

public class StudentService {

    private StudentService() {};
    static Student bestStudent(Student[] students) {

        Student bestStudent = null;

        double maxAverageGrade = 0;
        for (int i = 0; i < students.length; i++) {
            double currentAverage = students[i].getAverageGrade();
            if (currentAverage > maxAverageGrade) {
                maxAverageGrade = currentAverage;
                bestStudent = students[i];
            }
        }

        return bestStudent;
    }

    public static void sortBySurname(Student[] students) {
        boolean wasSorted;
        do {
            wasSorted = false;
            for (int i = 0; i < students.length - 1; i++) {
                if (students[i].getSurname().compareTo(students[i + 1].getSurname()) > 0) {
                    Student tmp = students[i];
                    students[i] = students[i + 1];
                    students[i + 1] = tmp;
                    wasSorted = true;
                }
            }
        } while (wasSorted);
    }
}
