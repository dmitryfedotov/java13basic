package homework.homework3part1;

public class DayOfWeek {
    private byte number;
    private String title;

    public DayOfWeek(byte number, String title) {
        this.number = number;
        this.title = title;
    }

    @Override
    public String toString() {
        return number + " " + title;
    }
}
