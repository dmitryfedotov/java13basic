package misc;

import java.util.*;

public class LinkedHashSetTest {
    public static void main(String[] args) {
        System.out.println(getAllInterfaces(new TreeSet<>()));
    }

    public static Set<Class<?>> getAllInterfaces(Object o) {
        Set<Class<?>> interfacesList = new LinkedHashSet<>();
        Class<?> curClass = o.getClass();
        while (curClass != Object.class) {
            Class<?> superClass = curClass.getSuperclass();
            Class<?>[] curClassInterfaces = curClass.getInterfaces();
            if (curClassInterfaces.length > 0) {
                interfacesList.addAll(List.of(curClassInterfaces));
            }
            curClass = superClass;
        }

        Class<?>[] curElInterfaces;
        for (Class<?> el: interfacesList
             ) {
            if (el.getInterfaces().length > 0)
                interfacesList.addAll(List.of(el.getInterfaces()));
        }

        return interfacesList;
    }
}
