-- всегда идет логирование
delete from books
where id = 7;

truncate table books; -- удаляет все без условий и логов

drop table books; -- удаляет таблицу