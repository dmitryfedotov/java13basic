-- DDL - data definition language
-- DML - data manipulation language
create table books(
    id serial primary key,
    title varchar(30) not null ,
    author varchar(50) not null ,
    date_added timestamp not null
);

select *
from books;

insert into books (title, author, date_added)
values ('Недоросль', 'Д.И. Фонвизин', now());
commit;
--nextval('books_id_seq') - получить следующее значение автоинкремента

insert into books (title, author, date_added)
values ('Путешествие из Петербурга в Москву', 'А.Н. Радищев', now() - interval '24h');

rollback;
alter table books
alter column title type varchar(100);

delete from books
where id = 3;