insert into books (title, author, date_added)
values ('Доктор Живаго', 'Б.Л. Пастернак', now());

insert into books (title, author, date_added)
values ('Сестра моя - жизнь', 'Б.Л. Пастернак', now());

select *
from books
order by date_added desc
limit 2;

select distinct(author)
from books;

select author
from books
where title like '%Недоросль%';

select *
from books
where lower(trim(author)) like lower('%Пастернак%');

select max(id)
from books;

select title, count (*)
from books
group by title;

select *
from books
where lower(trim(author)) like lower('%Пастернак%') or lower(trim(author)) like lower('%Радищев%')
order by date_added desc;

select *
from books
where lower(trim(author)) like lower('%Радищев%')
    and date_added <= now() - interval '24h';