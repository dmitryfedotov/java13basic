update books
set date_added = '2022-09-01 12:12:12.123456'
where id = 2;

--create table if not exists reviews (
create table reviews (
                         id bigserial primary key,
                         book_id int references books (id),
                         reviewer varchar(100) not null ,
                         rating integer not null,
                         comment text null
);