--create table if not exists reviews (
create table reviews (
    id bigserial primary key,
    book_id int references books (id),
    reviewer varchar(100) not null ,
    rating integer not null,
    comment text null
);

insert into reviews (book_id, reviewer, rating)
values (2, 'qweqwe', 5);

insert into reviews (book_id, reviewer, rating)
values (2, 'qweqwe', 3);

insert into reviews (book_id, reviewer, rating)
values (8, 'qweqwe', 4);

select count(*) as количество --"количество отзывов"
from reviews;

select count(distinct book_id)
from reviews;

select book_id, count(*)
from reviews
group by book_id;

select *
from books b inner join reviews r on b.id = r.book_id;

select *
from books b left join public.reviews r on b.id = r.book_id
where r.id is null;

select title, avg(rating)
from books b inner join public.reviews r on b.id = r.book_id
group by title;