package professional.week1.task3;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

public class FileReadWrite {
    private static final String PKG_DIRECTORY = "C:\\Users\\Дмитрий\\IdeaProjects\\java13basic\\src\\professional"
            + "\\week1\\task3";
    private static final String OUTPUT_FILENAME = "output.txt";
    private static final String INPUT_FILENAME = "input.txt";

    public static void main(String[] args) {

    }

    public static void readAndWriteFile() throws IOException {
        Scanner scanner = new Scanner(PKG_DIRECTORY + "\\" + INPUT_FILENAME);
        Writer writer = new FileWriter(PKG_DIRECTORY + "\\" + OUTPUT_FILENAME);

        // ресурс внутри блока try with resources уже нельзя менять
        try (scanner; writer) {
            //scanner = null;
            while (scanner.hasNext())
                writer.write(scanner.nextLine() + "\n");
        }
    }
}
