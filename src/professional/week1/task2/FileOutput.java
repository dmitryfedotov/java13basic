package professional.week1.task2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileOutput {
    private static final String PKG_DIRECTORY = "C:\\Users\\Дмитрий\\IdeaProjects\\java13basic\\src\\professional"
        + "\\week1\\task2";
    private static final String OUTPUT_FILENAME = "output.txt";

    public static void main(String[] args) {
        try (Writer writer = new FileWriter(PKG_DIRECTORY + "\\" + OUTPUT_FILENAME)) {
            writer.write("hello");
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
