package professional.week1.exceptions;

import java.util.Scanner;

public class SimpleException {
    public static void main(String[] args) throws MyArithmeticException {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        toDivide(100, n);
    }

    public static void toDivide(int a, int b) throws MyArithmeticException {
        try {
            System.out.println(a / b);
        } catch (ArithmeticException e) {
            throw new MyArithmeticException("Деление на 0. " + e.getMessage());
        } finally {
            System.out.println("the end");
        }
    }
}
