package professional.week1.exceptions;

public class MyArithmeticException extends Exception {

    public MyArithmeticException() {
    }

    public MyArithmeticException(String s) {
        super(s);
    }
}
