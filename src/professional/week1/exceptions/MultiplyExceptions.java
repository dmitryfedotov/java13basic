package professional.week1.exceptions;

public class MultiplyExceptions {
    public static void main(String[] args) {
        try {
            simpleThrowRuntimeException();
            toDivideWithException(100, 0);
            methodThrowsOutOfBound();
        } catch (MyArithmeticException e) {
            System.out.println("my");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("index");
        } catch (RuntimeException e) {
            System.out.println("runtime");
        } // catch (RuntimeException | ArithmeticException)
    }

    public static void simpleThrowRuntimeException() {
        throw new RuntimeException();
    }

    public static void methodThrowsOutOfBound() {
        int[] array = new int[10];
        System.out.println(array[10]);
    }

    public static void toDivideWithException(int a, int b) throws MyArithmeticException {
        try {
            System.out.println(a / b);
        } catch (ArithmeticException e) {
            throw new MyArithmeticException("");
        }
    }
}
