package professional.week1.task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileReader {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(new File("input.txt"))) {
            List<String> lines = new ArrayList<>();
            while (scanner.hasNextLine())
                lines.add(scanner.nextLine());
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
    }

    public static void simpleTry() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File("input.txt"));
            ArrayList<String> lines = new ArrayList<>();
            while (scanner.hasNextLine())
                lines.add(scanner.nextLine());
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } finally {
            if (scanner != null)
                scanner.close();
        }
    }
}
