package professional.week3.annotation;

import java.util.HashSet;
import java.util.Set;

public class Annotations implements Summable {
    @Override
    public int sum(int a, int b) {
        return 0;
    }

    @SuppressWarnings({"rawtypes, unchecked"})
    public static void test() {
        Set set = new HashSet();
        set.add(1);
        System.out.println(set.size());
    }
}
