package professional.week3.annotation;

import java.util.Arrays;

public class AnnotationReflexion {
    public static void main(String[] args) {
        writeDescription(MyPerfectClass.class);
    }

    public static void writeDescription(Class<?> clazz) {
        if (!clazz.isAnnotationPresent(ClassDescription.class))
            return;

        ClassDescription classDescription = clazz.getAnnotation(ClassDescription.class);
        System.out.println(classDescription.author());
        System.out.println(classDescription.date());
        System.out.println(classDescription.currentRevision());
        System.out.println(Arrays.toString(classDescription.reviewers()));
    }
}
