package professional.week3.annotation;

public interface Summable {
    int sum(int a, int b);
}
