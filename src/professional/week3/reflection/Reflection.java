package professional.week3.reflection;

public class Reflection {
    public static void main(String[] args) {
        // 1. .class - псевдополе
        Class<String> c1 = String.class;

        // 2. getClass() of Object
        CharSequence qwe = "qweqwe";
        Class<? extends CharSequence> c2 = qwe.getClass(); // in sout we get class java.lang.String

        // 3.
        try {
            Class<?> integerClass = Class.forName("java.lang.Integer");
        } catch (ClassNotFoundException e) {
            System.out.println("Class can not be found");
        }
    }
}
