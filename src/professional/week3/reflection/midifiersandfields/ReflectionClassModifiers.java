package professional.week3.reflection.midifiersandfields;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/*
getModifiers() - все модификаторы, с которыми был объявлен класс

Работа с полями
getFields() - все публичные поля класса или интерфейса, включая унаследованные
SecurityException - Бросается в случае запрета доступу к пакету, в котором класс

getDeclareFields() - все поля класса или интерфейса, исключая унаследованные
 */
public class ReflectionClassModifiers {
    public static void main(String[] args) throws IllegalAccessException {
        //System.out.println(Modifier.isPublic(ReflectionClassModifiers.class.getModifiers()));
        //printAllClassFields(Task2.class);

        Task2 task2 = new Task2();
        printAllClassFieldsWithValues(Task2.class, task2);
    }

    public static void printAllClassFields(Class<?> clazz) {
        for (Field field : clazz.getDeclaredFields()) {
            int mods = field.getModifiers();
            if (Modifier.isPublic(mods))
                System.out.println("public ");

            if (Modifier.isProtected(mods))
                System.out.println("protected ");

            if (Modifier.isPrivate(mods))
                System.out.println("private ");

            if (Modifier.isStatic(mods))
                System.out.println("static ");

            if (Modifier.isFinal(mods))
                System.out.println("final ");

            System.out.println(field.getType().getName() + " ");
        }
    }

    public static void printAllClassFieldsWithValues(Class<?> clazz, Task2 task2) throws IllegalAccessException {
        printAllClassFields(clazz);

        System.out.println("Значения:");
        for (Field field : clazz.getDeclaredFields()) {
            int mods = field.getModifiers();
            if (Modifier.isPrivate(mods))
                field.setAccessible(true); // обязательно для private
            System.out.println(field.get(task2));
        }
    }
}
