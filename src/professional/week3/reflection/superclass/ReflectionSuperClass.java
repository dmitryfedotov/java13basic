package professional.week3.reflection.superclass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReflectionSuperClass {
    public static void main(String[] args) {
//        for (Class<?> cls: D.class.getInterfaces()) {
//            System.out.println(cls.getName()); // sout show only B
//        }

        List<Class<?>> list = getAllInterfaces(D.class);
        System.out.println(list);
    }

    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();

        while (cls != Object.class) {
            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
            cls = cls.getSuperclass();
        }
        return interfaces;
    }
}
