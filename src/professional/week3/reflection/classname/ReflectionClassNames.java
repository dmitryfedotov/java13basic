package professional.week3.reflection.classname;

import java.util.AbstractMap;
import java.util.HashMap;

public class ReflectionClassNames {
    public static void main(String[] args) {
        // getName() - полное имя класса, например java.lang...
        // getSimpleName() - короткое имя класса без указания пакетов
        // getPackage().getName() - только имя пакета

        //printNamesForClass(int.class, "int primitives");
        printNamesForClass(String.class, "String");
        printNamesForClass(java.util.HashMap.SimpleEntry.class, "java.util.HashMap.SimpleEntry");
        printNamesForClass(new java.io.Serializable() {}.getClass(), "java.io.Serializable");
    }

    public static void printNamesForClass(Class<?> clazz, String label) {
        System.out.println(label + ":");
        System.out.println("getName(): " + clazz.getName());
        System.out.println("getSimpleName(): " + clazz.getSimpleName());
        System.out.println("getPackage().getName(): " + clazz.getPackage().getName());
    }
}
