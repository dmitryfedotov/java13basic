package professional.week3.reflection.methods;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ReflectionClassMethods {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<Task4> cls = Task4.class;
        Constructor<Task4> constructor = cls.getDeclaredConstructor(int.class, String.class);

        Task4 result = constructor.newInstance(1, "qwe");
        System.out.println(result);
    }
}
