package professional.week2.genericcollections.task2;

public class Main {
    public static void main(String[] args) {
        Pair<String, Integer> pair = new Pair<>();
        pair.setFirst("test");
        pair.setSecond(123);
        System.out.println(pair);
    }
}
