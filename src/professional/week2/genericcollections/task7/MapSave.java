package professional.week2.genericcollections.task7;

import java.util.HashMap;
import java.util.Map;

public class MapSave {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "first");
        map.put(2, "second");
        map.put(3, "third");

        System.out.println(map.get(2));
    }
}
