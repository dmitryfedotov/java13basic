package professional.week2.genericcollections.task1;

//public class Pair <T> - один тип на две переменных
//public class Pair <T extends String, U extends Number> - корявый пример с наследованием
public class Pair <T, U> {
    public T first;
    public U second;

    public void print() {
        System.out.println("First " + first + ", second " + second);
    }
}
