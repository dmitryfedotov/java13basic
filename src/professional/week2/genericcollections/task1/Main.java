package professional.week2.genericcollections.task1;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Pair<String, Double> pair = new Pair<>();
        pair.print();
        Pair<String, Date> pair1 = new Pair<>();
        pair.print();
    }
}
