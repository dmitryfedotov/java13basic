package professional.week2.genericcollections.task8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordCount {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        list.add("слон");
        list.add("слон");
        list.add("кот");
        list.add("мышь");
        list.add("кот");
        list.add("кот");

        System.out.println(getWordsCount(list));
    }

    private static Map<String, Integer> getWordsCount(List<String> list) {
        Map<String, Integer> map = new HashMap<>();

        for (String str : list) {
            int count = 0;
            if (map.containsKey(str))
                count = map.get(str);
            map.put(str, ++count);
        }

        return map;
    }
}
