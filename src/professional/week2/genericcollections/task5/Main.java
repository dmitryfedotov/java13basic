package professional.week2.genericcollections.task5;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<String> set1 = new HashSet<>();
        set1.add("qwe");
        set1.add("asdasd");
        set1.add("zxc");
        set1.add("sdfsdf");
        System.out.println(set1);

        System.out.println(ConvertHashSet.convertHashSet(set1));
    }
}
