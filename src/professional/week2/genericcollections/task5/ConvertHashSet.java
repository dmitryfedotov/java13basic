package professional.week2.genericcollections.task5;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class ConvertHashSet {
    private ConvertHashSet() {};

    public static <T>TreeSet<T> convertHashSet(Set<T> from) {

//        for (T element: from) {
//            toReturn.add(element);
//        }

        // toReturn.addAll(from);
        TreeSet<T> toReturn = new TreeSet<>(from);

        return toReturn;
    }
}
