package professional.week2.genericcollections.task4;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Boolean> list1 = new ArrayList<>();
        list1.add(true);
        list1.add(true);
        list1.add(false);

        System.out.println(ListUtil.countIf(list1, true));

        List<String> list2 = new ArrayList<>();
        list2.add("abc");
        list2.add("qqq");
        list2.add("qwe");
        list2.add("qwe");
        System.out.println(ListUtil.countIf(list2, "qwe"));
        // String pool формируется на этапе компиляции
        // String str1 = "qwe"; - будет в String pool
        // String str2 = "qwe" + str1. - Не будет String pool, т.к. она формируется только
        // во время выполнения.
        String str3 = (new String("qwe")).intern(); // принудительно помещает строку в String pool
    }
}
