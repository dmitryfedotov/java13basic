package professional.week2.genericcollections.task4;

import java.util.List;

public class ListUtil {
    private ListUtil() {};

    public static <T> int countIf(List<T> from, T elem) {
        int counter = 0;
        for (T element: from) {
            if (element.equals(elem))
                counter++;
        }

        return counter;
    }
}
