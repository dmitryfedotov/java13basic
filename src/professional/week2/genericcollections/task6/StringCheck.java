package professional.week2.genericcollections.task6;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class StringCheck {
    public static void main(String[] args) {

    }

    public static boolean checkString(String sequence) {
        if (sequence.length() < 26)
            return false;

        Set<Character> set = new TreeSet<>();
        for (char ch: sequence.toCharArray()) {
            set.add(ch);
        }

        return set.size() == 26;
    }
}
