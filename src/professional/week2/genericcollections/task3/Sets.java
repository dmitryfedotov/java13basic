package professional.week2.genericcollections.task3;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Sets {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(0);
        set1.add(0);
        set1.add(1);
        set1.add(2);
        set1.add(3);

        Set<Integer> set2 = new HashSet<>();
        set2.add(4);
        set2.add(3);
        set2.add(2);
        set2.add(2);

        Set<Integer> set3 = new HashSet<>();
        set1.retainAll(set2);
        System.out.println(set1);
        set1.forEach(e -> System.out.println(e));
        set1.forEach(System.out::println);

        // boolean. Есть пересечение или нет.
        Collections.disjoint(set1, set2);
    }
}
