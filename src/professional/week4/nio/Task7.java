package professional.week4.nio;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Task7 {
    public static void main(String[] args) {
        Path path = Paths.get("src/testToWrite.txt");
        try {
            Files.writeString(path, "qweqwe");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
