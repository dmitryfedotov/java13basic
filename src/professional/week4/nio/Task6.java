package professional.week4.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Task6 {
    public static void main(String[] args) {
        Path path = Paths.get("src/test1.txt");
        try {
            List<String> content = Files.readAllLines(path);
            System.out.println(content);
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            Files.lines(path).forEach(System.out::println);
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
