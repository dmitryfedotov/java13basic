package professional.week4.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Task3 {
    public static void main(String[] args) {
        Path source = Paths.get("src/test.txt");
        Path target = Paths.get("src/test1.txt");
        try {
            Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(new String(Files.readAllBytes(target)));
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
