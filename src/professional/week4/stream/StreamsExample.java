package professional.week4.stream;

import java.util.ArrayList;
import java.util.List;

public class StreamsExample {
    public static void main(String[] args) {
        List<String> myPlaces = List.of("qwe", "asd", "qwer");
        myPlaces
                .stream()
                .filter(s -> s.startsWith("qwe"))
                //.map(s -> s.toUpperCase())
                // :: - ссылка на метод, который эквивалентен лямбде
                .map(String::toUpperCase)
                .sorted()
                .forEach(System.out::println);
    }
}
