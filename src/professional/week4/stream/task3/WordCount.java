package professional.week4.stream.task3;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WordCount {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        list.add("слон");
        list.add("слон");
        list.add("кот");
        list.add("мышь");
        list.add("кот");
        list.add("кот");

        // 1 способ
        Set<String> unique = new HashSet<>(list);
        for (String key: unique) {
            System.out.println(key + ": " + Collections.frequency(list, key));

        }


        // 2 способ
        Map<String, Long> frequencyMap = list.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(frequencyMap);

        // 3 способ
        Map<String, Integer> counts = list
                .parallelStream()
                .collect(Collectors.toConcurrentMap(w -> w, w -> 1, Integer::sum));
        System.out.println(counts);

    }
}
