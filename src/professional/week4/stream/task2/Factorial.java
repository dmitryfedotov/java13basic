package professional.week4.stream.task2;

import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Factorial {
    public static void main(String[] args) {
        System.out.println(fact(5));
        System.out.println(fact2(30));
    }

    public static int fact(int n) {
        return IntStream
                .rangeClosed(1, n)
                .reduce(1, (x, y) -> x * y);
    }

    public static BigInteger fact1(int n) {
        return IntStream.rangeClosed(1, n)
                .mapToObj(BigInteger::valueOf)
                .reduce(BigInteger.ONE, BigInteger::multiply);
    }

    public static BigInteger fact2(int n) {
        return Stream.iterate(BigInteger.ONE, i -> i.add(BigInteger.ONE))
                .limit(n)
                .reduce(BigInteger.ONE, BigInteger::multiply);
    }

}