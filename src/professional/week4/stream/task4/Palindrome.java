package professional.week4.stream.task4;

public class Palindrome {
    public static void main(String[] args) {

    }

    public static boolean isPalindrome(String s) {
        StringBuilder leftToRight = new StringBuilder();
        s.chars()
                .filter(Character::isLetterOrDigit)
                .map(Character::toLowerCase)
                .forEach(leftToRight::appendCodePoint);

        StringBuilder rightToLeft = new StringBuilder(leftToRight).reverse();
        return rightToLeft.toString().equals(leftToRight.toString());
    }
}
