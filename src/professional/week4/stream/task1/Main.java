package professional.week4.stream.task1;

import professional.week4.functional.task2.Square;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Square square = x -> x * x;
        List<Integer> list = List.of(1, 2, 3, 4, 5);

        list.stream()
                .map(square::calculate)
                .forEach(System.out::println);
    }
}
