package professional.week4.functional.task2;

public class Main {
    public static void main(String[] args) {
        Square square = x -> x * x;
        System.out.println(square.calculate(3));
    }
}
