package professional.week4.functional.task4;

public class Main {
    public static void main(String[] args) {
        ReverseInterface reverseInterface = s -> {
            StringBuilder builder = new StringBuilder(s);
            return builder.reverse().toString();
        };

        System.out.println(reverseInterface.reverse("12345"));
    }
}
