package professional.week4.functional.task3;

public class Main {
    public static void main(String[] args) {
        Pi pi = () -> Math.PI;
        System.out.println(pi.get());
    }
}
