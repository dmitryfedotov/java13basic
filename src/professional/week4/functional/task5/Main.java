package professional.week4.functional.task5;

public class Main {
    public static void main(String[] args) {
        MyGenericInterface<String> reverseString = s -> new StringBuilder(s).reverse().toString();
        MyGenericInterface<Integer> factorial = n -> {
            int result = 1;
            for (int i = 2; i <= n; i++) {
                result *= i;
            }

            return result;
        };

        System.out.println(reverseString.func("12345"));
        System.out.println(factorial.func(5));
    }
}
