package oopcourse.part1;

public class Location {
    public int row;
    public int column;
    public double maxValue;

    public static Location locateLargest(double[][] a) {
        double max = Integer.MIN_VALUE;
        int row = 0;
        int column = 0;

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                if (a[i][j] > max) {
                    max = a[i][j];
                    row = i;
                    column = j;
                }
            }
        }

        Location result = new Location();
        result.row = row;
        result.column = column;
        result.maxValue = max;

        return result;
    }
}
