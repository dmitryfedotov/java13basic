package oopcourse.part1;

public class Test {
    public static void main(String[] args) {
        new Person().printPerson();
        new Student().printPerson();
    }
}

class Student extends Person {
    private String getInfo() {
        return "Студент";
    }
}

class Person {
    private String getInfo() {
        return "Человек";
    }

    public void printPerson() {
        System.out.println(getInfo());
    }
}
