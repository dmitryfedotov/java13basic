package oopcourse.part1;

import java.util.Locale;
import java.util.Scanner;

public class LocationTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.ENGLISH);

        int n = scanner.nextInt();
        int m = scanner.nextInt();

        double[][] a = new double[n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = scanner.nextDouble();
            }
        }

        Location location = Location.locateLargest(a);
        System.out.println(location.maxValue + ": " + location.row + ", " + location.column);
    }
}
