package oopcourse.part1;

public class StopWatchTest {
    final static int ARRAY_LENGTH = 100_000;
    public static void main(String[] args) {
        int[] array = new int[ARRAY_LENGTH];

        for (int i = 0; i < ARRAY_LENGTH; i++) {
            array[i] = (int) (Math.random() * 100_000);
        }

        StopWatch stopWatch = new StopWatch();
        for (int i = 0; i < array.length - 1; i++) {
            sortArray(array, i);
        }

        stopWatch.stop();
        //System.out.println(Arrays.toString(array));
        System.out.println(stopWatch.getElapsedTime());
    }

    static void sortArray(int[] array, int startPosition) {
        int min = Integer.MAX_VALUE;
        int minIndex = -1;
        for (int i = startPosition; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
                minIndex = i;
            }
        }

        if (minIndex != -1) {
            array[minIndex] = array[startPosition];
            array[startPosition] = min;
        }
    }
}
