package oopcourse.part1;

public class MyInteger {
    private int value;

    public MyInteger(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public boolean isEven() {
        return isEven(value);
    }

    public boolean isOdd() {
        return !isEven(value);
    }

    public boolean isPrime() {
        return isPrime(value);
    }

    static boolean isEven(int value) {
        return value % 2 == 0;
    }

    static boolean isOdd(int value) {
        return !isEven(value);
    }

    static boolean isPrime(int value) {
        for (int i = 2; i < value / 2; i++) {
            if (value % i == 0)
                return false;
        }

        return true;
    }

    static boolean isEven(MyInteger myInteger) {
        return isEven(myInteger.value);
    }

    static boolean isOdd(MyInteger myInteger) {
        return isOdd(myInteger.value);
    }

    static boolean isPrime(MyInteger myInteger) {
        return isPrime(myInteger.value);
    }

    boolean equals(int number) {
        return this.value == number;
    }

    boolean equals(MyInteger myInteger) {
        return this.value == myInteger.value;
    }

    static int parseInt(char[] chars) {
        return Integer.parseInt(String.valueOf(chars));
    }

    static int parseInt(String string) {
        return Integer.parseInt(string);
    }
 }
