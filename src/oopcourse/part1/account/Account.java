package oopcourse.part1.account;

import java.util.Date;

public class Account {
    private int id;
    private double balance;
    private static double annualInterestRate = 0;
    private Date dateCreated;

    public Account() {
        //this.id = 0;
        //this.balance = 0;
        //this.dateCreated = new Date();
    }

    public Account(int id, double balance) {
        this.id = id;
        this.balance = balance;
        this.dateCreated = new Date();
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        if (balance <= 0)
            throw new IllegalArgumentException("Balance must be positive");
        this.balance = balance;
    }

    public static double getAnnualInterestRate() {
        return annualInterestRate;
    }

    public static void setAnnualInterestRate(double annualInterestRate) {
        if (annualInterestRate <= 0)
            throw new IllegalArgumentException("Rate must be positive");

        Account.annualInterestRate = annualInterestRate;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public double getMonthlyInterest() {
        return balance * annualInterestRate / 12 / 100;
    }

    public void withdraw(double sum) {
        if (sum <= this.balance)
            this.balance -= sum;
        else
            throw new IllegalArgumentException("Недостаточно средств");
    }

    public void deposit(double sum) {
        this.balance += sum;
    }

}
