package oopcourse.part1.account;

import java.util.ArrayList;
import java.util.List;

public class AccountWithTransactions extends Account {
    private String name;
    private List<Transaction> transactions = new ArrayList<>();

    public AccountWithTransactions(int id, double balance, String name) {
        super(id, balance);
        this.name = name;
        Integer[] array = {1, 2, 3};
    }
    @Override
    public void withdraw(double sum) {
        super.withdraw(sum);

        Transaction transaction = new Transaction(
                '-', sum, getBalance(), "Снял денежков");
        transactions.add(transaction);
    }

    @Override
    public void deposit(double sum) {
        super.deposit(sum);

        Transaction transaction = new Transaction(
                '+', sum, getBalance(), "Положил денежков");
        transactions.add(transaction);
    }

    public void printTransactions() {
        for (int i = 0; i < transactions.size(); i++) {
            System.out.println(transactions.get(i));
        }
    }
}
