package oopcourse.part1.account;

public class AccountWithTransactionsTest {
    public static void main(String[] args) {
        AccountWithTransactions account = new AccountWithTransactions(1122, 1000, "Герман");
        Account.setAnnualInterestRate(5.5);
        account.deposit(300);
        account.deposit(400);
        account.deposit(500);

        account.withdraw(500);
        account.withdraw(400);
        account.withdraw(200);

        account.printTransactions();
    }
}
