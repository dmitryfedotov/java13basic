package oopcourse.part1.account;

import java.util.Scanner;

public class AccountTest {
    public static void main(String[] args) {
        Account[] accounts = new Account[10];
        accounts[0] = new Account(0, 20000);
        accounts[1] = new Account(1, 20000);
        accounts[2] = new Account(2, 20000);
        accounts[3] = new Account(3, 20000);
        accounts[4] = new Account(4, 20000);
        accounts[5] = new Account(5, 20000);
        accounts[6] = new Account(6, 20000);
        accounts[7] = new Account(7, 20000);
        accounts[8] = new Account(8, 20000);
        accounts[9] = new Account(9, 20000);

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Введите ID: ");
            boolean idIsCorrect = false;
            int accountId = -1;
            while (accountId == -1) {
                int currentInput = scanner.nextInt();
                if (currentInput >= 0 && currentInput <= 9)
                    accountId = currentInput;
                else {
                    System.out.println("Введен некорректный ID");
                    System.out.print("Введите ID: ");
                }
            }

            System.out.println("Основное меню");
            System.out.println("1: проверить баланс счета");
            System.out.println("2: снять со счета");
            System.out.println("3: положить на счет");
            System.out.println("4: выйти");
            String qwe = new String(new char[] {'a'});
            byte operation = -1;
            while (true) {
                System.out.print("Введите пункт меню: ");
                byte currentOperation = scanner.nextByte();
                if (currentOperation >= 1 && currentOperation <= 4) {
                    if (currentOperation == 1)
                        System.out.println("Баланс равен " + accounts[accountId].getBalance());
                    else if (currentOperation == 2) {
                        System.out.print("Введите сумму для снятия со счета: ");
                        accounts[accountId].withdraw(scanner.nextDouble());
                    } else if (currentOperation == 3) {
                        System.out.print("Введите сумму для внесения на счет: ");
                        accounts[accountId].deposit(scanner.nextDouble());
                    } else if (currentOperation == 4)
                        break;
                    else
                        System.out.println("Некорректный номер операции");
                }
            }
        }
    }
}
