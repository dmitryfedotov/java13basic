package oopcourse.part1.arraylist;

import java.util.ArrayList;
import java.util.Scanner;

public class Task32 {
    final static int CAPACITY = 10;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] weights = new int[n];
        for (int i = 0; i < n; i++) {
            weights[i] = scanner.nextInt();
        }
        ArrayList<ArrayList<Integer>> list = new ArrayList<>();
        int freeSpace = 0;
//        ArrayList<Integer> container = null;
//        for (int i = 0; i < n; i++) {
//            if (freeSpace - weights[i] < 0) {
//                container = new ArrayList<>();
//                list.add(container);
//                freeSpace = CAPACITY;
//            }
//            container.add(weights[i]);
//            freeSpace -= weights[i];
//        }

        for (int i = 0; i < n; i++) {
            ArrayList<Integer> chosenContainer = null;
            for (int j = 0; j < list.size(); j++) {
                ArrayList<Integer> currentContainer = list.get(j);
                int currentWeight = 0;
                for (int k = 0; k < currentContainer.size(); k++) {
                    currentWeight += currentContainer.get(k);
                }
                if (currentWeight + weights[i] <= CAPACITY) {
                    chosenContainer = currentContainer;
                    break;
                }
            }

            if (chosenContainer == null) {
                chosenContainer = new ArrayList<>();
                list.add(chosenContainer);
            }
            chosenContainer.add(weights[i]);
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
