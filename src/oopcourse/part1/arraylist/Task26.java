package oopcourse.part1.arraylist;

import java.util.ArrayList;
import java.util.Scanner;

public class Task26 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] matrix = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = (int) (Math.random() + 0.5);
            }
        }

        int maxInRow = 0;
        int maxInColumn = 0;

        for (int i = 0; i < n; i++) {
            int sumInRow = 0;
            int sumInColumn = 0;
            for (int j = 0; j < n; j++) {
                sumInRow += matrix[i][j];
                sumInColumn += matrix[j][i];
            }
            if (sumInRow > maxInRow)
                maxInRow = sumInRow;
            if (sumInColumn > maxInColumn) {
                maxInColumn = sumInColumn;
            }
        }

        ArrayList<Integer> rowsWithMax = new ArrayList<>();
        ArrayList<Integer> columnsWithMax = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int sumInRow = 0;
            int sumInColumn = 0;
            for (int j = 0; j < n; j++) {
                System.out.print(matrix[i][j] + " ");
                sumInRow += matrix[i][j];
                sumInColumn += matrix[j][i];
            }
            System.out.println();
            if (sumInRow == maxInRow)
                rowsWithMax.add(i);
            if (sumInColumn == maxInColumn) {
                columnsWithMax.add(i);
            }
        }

        System.out.println("Индекс строчки с наибольшим кол-вом единиц: " + rowsWithMax);
        System.out.println("Индекс столбца с наибольшим кол-вом единиц: " + columnsWithMax);
    }
}
