package oopcourse.part1.arraylist;

import java.util.ArrayList;
import java.util.Scanner;

public class Task29 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            int currentInput = scanner.nextInt();
            if (!list.contains(currentInput))
                list.add(currentInput);
        }

        System.out.println(list);
    }
}
