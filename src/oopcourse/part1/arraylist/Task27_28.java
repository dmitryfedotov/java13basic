package oopcourse.part1.arraylist;

import java.util.ArrayList;
import java.util.Scanner;

public class Task27_28 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(scanner.nextInt());
        }

        ArrayList<Integer> task27 = (ArrayList<Integer>) list.clone();
        sort(task27);
        System.out.println(task27);

        System.out.println(sum(list));

    }

    public static void sort(ArrayList<Integer> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            int min = list.get(i);
            int index = i;
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(j) < min) {
                    min = list.get(j);
                    index = j;
                }
            }

            if (index != i) {
                list.set(index, list.get(i));
                list.set(i, min);
            }
        }
    }

    public static double sum(ArrayList<Integer> list) {
        double sum = 0;
        for (int i = 0; i < list.size(); i++) {
            sum += list.get(i);
        }

        return sum;
    }
}
