package oopcourse.part1.arraylist;

import oopcourse.part1.account.Account;
import oopcourse.part1.geometric.Circle;

import java.util.ArrayList;
import java.util.Date;

public class Task24 {
    public static void main(String[] args) {
        ArrayList<Object> list = new ArrayList<>();
        list.add(new Account());
        list.add(new Date());
        list.add(new Circle());

        for (Object i:list) {
            System.out.println(i);
        }
    }
}
