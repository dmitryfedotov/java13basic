package oopcourse.part1.arraylist;

import java.util.ArrayList;

public class Task25 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < 50; i++) {
            list.add(i);
        }

        System.out.println(list);
        shuffle(list);
        System.out.println(list);
    }

    public static void shuffle(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            int newIndex = (int) (Math.random() * list.size());

            int tmp = list.get(i);
            list.set(i, list.get(newIndex));
            list.set(newIndex, tmp);
        }
    }
}
