package oopcourse.part1.arraylist;

import java.util.*;

public class Task23 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<Integer> list = new ArrayList<>();
        int value;
        do {
            value = scanner.nextInt(); // считывает входное значение

            if (value != 0)
                list.add(value); // добавляет значение, если его еще нет в списке

        } while (value != 0);

        System.out.println(max(list));
    }

    public static Integer max(ArrayList<Integer> list) {
        if (list.isEmpty())
            return null;

        return Collections.max(list);
    }
}
