package oopcourse.part1.arraylist;

import java.util.ArrayList;
import java.util.Scanner;

public class Task30 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> list1 = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            list1.add(scanner.nextInt());
        }
        for (int i = 0; i < 5; i++) {
            list2.add(scanner.nextInt());
        }

        System.out.println(union(list1, list2));

    }

    public static ArrayList<Integer> union(ArrayList<Integer> list1, ArrayList<Integer> list2) {
        ArrayList<Integer> result = new ArrayList<>();

        for (int i = 0; i < list1.size(); i++) {
            result.add(list1.get(i));
        }

        for (int i = 0; i < list2.size(); i++) {
            result.add(list2.get(i));
        }
        return result;
    }
}
