package oopcourse.part1.strings;

import java.util.Arrays;

public class MyString1 {
    private char[] chars;

    public MyString1(char[] chars) {
        this.chars = chars;
    }

    public char charAt(int index) {
        return chars[index];
    }

    public int length() {
        return chars.length;
    }

    public MyString1 substring(int begin, int end) {
        return new MyString1(Arrays.copyOfRange(this.chars, begin, end));
    }

    public void toLowerCase() {
        for (int i = 0; i < chars.length; i++) {
            chars[i] = Character.toLowerCase(chars[i]);
        }
    }

    public char[] toChars() {
        return chars;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MyString1))
            return false;

        if (((MyString1) obj).chars.length != chars.length)
            return false;

        for (int i = 0; i < chars.length; i++) {
            if (chars[i] != ((MyString1) obj).charAt(i))
                return false;
        }

        return true;
    }

    public static MyString1 valueOf(int i) {
        return new MyString1(digits(i));
    }

    private static char[] digits(int value) {
        int length = 1;
        int number = 10;
        while (number < value) {
            number *= 10;
            length++;
        }

        char[] digits = new char[length];
        for (int i = 0; i < length; i++) {
            int remains = value % 10;
            digits[length - 1 - i] = (char) (remains + '0');
            value = value / 10;
        }

        return digits;
    }

}
