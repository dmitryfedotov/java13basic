package oopcourse.part1.strings;

import java.util.Arrays;

public class StringService {
    private StringService(){};

    public static String[] split(String string, String separator) {
        String[] strings = new String[string.length()];
        int index = 0;
        StringBuilder currentString = new StringBuilder();

        for (int i = 0; i < string.length(); i++) {
            String currentSymbol = "" + string.charAt(i);
            if (currentSymbol.matches(separator)) {
                strings[index++] = currentString.toString();
                strings[index++] = currentSymbol;
                currentString.setLength(0);
            } else {
                currentString.append(currentSymbol);
            }
        }

        if (!currentString.isEmpty())
            strings[index++] = currentString.toString();

        return Arrays.copyOfRange(strings, 0, index);

    }

    public static void main(String[] args) {
        String[] strings = split("ab#12#453", "#");
        for (int i = 0; i < strings.length; i++) {
            System.out.println(strings[i]);

        }

        strings = split("a?b?gf#e", "[?#]");
        for (int i = 0; i < strings.length; i++) {
            System.out.println(strings[i]);
        }
    }
}
