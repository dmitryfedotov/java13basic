package oopcourse.part1.strings;

import java.util.Arrays;

public class MyString1Test {
    public static void main(String[] args) {
        MyString1 string1 = new MyString1(new char[]{'t', 'e', 'S', 't'});
        System.out.println(string1.charAt(2));
        System.out.println(string1.length());

        System.out.println(Arrays.toString(string1.substring(1, 3).toChars()));

        string1.toLowerCase();
        System.out.println(Arrays.toString(string1.toChars()));

        MyString1 string2 = MyString1.valueOf(12345);
        System.out.println(Arrays.toString(string2.toChars()));

        System.out.println(string1.equals(string2));

        MyString1 string3 = MyString1.valueOf(12345);
        System.out.println(string2.equals(string3));

    }
}
