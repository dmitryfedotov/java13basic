package oopcourse.part1.strings;

import java.util.Arrays;

public class MyString2 {
    private char[] chars;

    public MyString2(char[] chars) {
        this.chars = chars;
    }

    public MyString2 substring(int begin) {
        return new MyString2(Arrays.copyOfRange(this.chars, begin, chars.length));
    }

    public void toUpperCase() {
        for (int i = 0; i < chars.length; i++) {
            chars[i] = Character.toUpperCase(chars[i]);
        }
    }

    public char[] toChars() {
        return chars;
    }

    public static MyString2 valueOf(boolean value) {
        char[] chars;
        if (value)
            chars = new char[]{'t', 'r', 'u', 'e'};
        else
            chars = new char[]{'f', 'a', 'l', 's', 'e'};

        return new MyString2(chars);
    }
}
