package oopcourse.part1.strings;

public class MyCharacter {
    private char value;

    public MyCharacter(char value) {
        this.value = value;
    }

    public char charValue() {
        return value;
    }

    public int compareTo(MyCharacter anotherCharacter) {
        if (this.value == anotherCharacter.value)
            return 0;
        else if (this.value > anotherCharacter.value)
            return 1;
        else
            return -1;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MyCharacter))
            return false;

        return this.value == ((MyCharacter) obj).value;
    }

    public boolean isDigit() {
        return isDigit(this.value);
    }

    public static boolean isDigit(char ch) {
        return (ch >= '0' && ch <= '9');
    }

    public static boolean isLetter(char ch) {
        return (ch >= 'a' && ch <= 'z'
            || ch >= 'A' && ch <= 'Z');
    }

    public static boolean isLetterOrDigit(char ch) {
        return isDigit(ch) || isLetter(ch);
    }

    public static boolean isUpperCase(char ch) {
        return ch >= 'A' && ch <= 'Z';
    }

    public static boolean isLowerCase(char ch) {
        return ch >= 'a' && ch <= 'z';
    }

    public static char toUpperCase(char ch) {
        if (isUpperCase(ch))
            return ch;
        else
            return (char) (ch + 'A' - 'a');
    }

    public static char toLowerCase(char ch) {
        if (isUpperCase(ch))
            return (char) (ch - 'A' + 'a');
        else
            return ch;
    }
}
