package oopcourse.part1.strings;

public class MyCharacterTest {
    public static void main(String[] args) {
        System.out.println(MyCharacter.isUpperCase('G'));
        System.out.println(MyCharacter.isUpperCase('g'));
        System.out.println(MyCharacter.toUpperCase('L'));
        System.out.println(MyCharacter.toUpperCase('l'));
    }
}
