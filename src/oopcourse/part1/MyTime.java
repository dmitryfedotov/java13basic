package oopcourse.part1;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MyTime {
    private int hour;
    private int minute;
    private int second;

    public MyTime() {
        setTime(System.currentTimeMillis());
    }

    public MyTime(long elapsedTime) {
        setTime(elapsedTime);
    }

    public MyTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    public void setTime(long elapsedTime) {
        Date date = new Date(elapsedTime);
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        this.hour = calendar.get(GregorianCalendar.HOUR);
        this.minute = calendar.get(GregorianCalendar.MINUTE);
        this.second = calendar.get(GregorianCalendar.SECOND);
    }

    public void printTime() {
        System.out.printf("%s:%s:%s", this.hour, this.minute, this.second);
        System.out.println();
    }
}
