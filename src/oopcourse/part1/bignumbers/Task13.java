package oopcourse.part1.bignumbers;

import java.math.BigInteger;

public class Task13 {
    public static void main(String[] args) {
        BigInteger bigInteger = new BigInteger("" + Long.MAX_VALUE);

        int count = 0;
        while (count != 1) {
            if (isPrime(bigInteger)) {
                System.out.println(bigInteger);
                count++;
            }
            bigInteger = bigInteger.add(BigInteger.ONE);
        }
    }

    public static boolean isPrime(BigInteger bigInteger) {
        BigInteger n = bigInteger.sqrt();
        BigInteger i = BigInteger.TWO;

        while (i.compareTo(n) == -1) {
            if (bigInteger.remainder(i).equals(BigInteger.ZERO))
                return false;
            i = i.add(BigInteger.ONE);
        }

        return true;
    }
}
