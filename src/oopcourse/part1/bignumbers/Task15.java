package oopcourse.part1.bignumbers;

import java.math.BigInteger;

public class Task15 {
    public static void main(String[] args) {
        BigInteger bigInteger = new BigInteger("" + Long.MAX_VALUE);

        int count = 0;
        BigInteger five = new BigInteger("5");
        BigInteger six = new BigInteger("6");
        while (count != 10) {
            if (bigInteger.remainder(five).equals(BigInteger.ZERO)
                || bigInteger.remainder(six).equals(BigInteger.ZERO)) {
                System.out.println(bigInteger);
                count++;
            }
            bigInteger = bigInteger.add(BigInteger.ONE);
        }
    }
}
