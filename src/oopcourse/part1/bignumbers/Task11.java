package oopcourse.part1.bignumbers;

import java.math.BigInteger;

public class Task11 {
    public static void main(String[] args) {
        String initialNumber = "1";
        for (int i = 0; i < 50; i++) {
            initialNumber += "0";
        }
        BigInteger bigInteger = new BigInteger(initialNumber);

        int count = 0;
        BigInteger three = new BigInteger("3");
        while (count != 10) {
            if (bigInteger.remainder(BigInteger.TWO).equals(BigInteger.ZERO)
                && bigInteger.remainder(three).equals(BigInteger.ZERO)) {
                System.out.println(bigInteger);
                count++;
            }
            bigInteger = bigInteger.add(BigInteger.ONE);
        }
    }
}
