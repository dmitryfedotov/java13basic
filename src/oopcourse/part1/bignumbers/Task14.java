package oopcourse.part1.bignumbers;

import java.math.BigInteger;

public class Task14 {
    public static void main(String[] args) {
        int count = 0;
        int pow = 2;
        while (count < 50) {
            BigInteger bigInteger = BigInteger.TWO.pow(pow++).subtract(BigInteger.ONE);
            if (Task13.isPrime(bigInteger)) {
                System.out.println(bigInteger);
                count++;
            }
        }
    }
}
