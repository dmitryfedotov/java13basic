package oopcourse.part1.bignumbers;

import java.math.BigInteger;

public class Task12 {
    public static void main(String[] args) {
        BigInteger bigInteger = new BigInteger("" + Long.MAX_VALUE);

        int count = 0;
        while (count != 1) {
            if (bigInteger.sqrtAndRemainder()[1].equals(BigInteger.ZERO)) {
                System.out.println(bigInteger);
                count++;
            }
            bigInteger = bigInteger.add(BigInteger.ONE);
        }
    }
}
