package oopcourse.part1;

import java.util.Date;

public class DateTest {
    public static void main(String[] args) {
        Date date;

        date = new Date(10000);
        System.out.println(date);

        date = new Date(100000);
        System.out.println(date);

        date = new Date(1000000);
        System.out.println(date);

        date = new Date(10000000);
        System.out.println(date);

        date = new Date(100000000);
        System.out.println(date);

        date = new Date(1000000000);
        System.out.println(date);

        date = new Date(10000000000L);
        System.out.println(date);

        date = new Date(100000000000L);
        System.out.println(date);
    }
}
