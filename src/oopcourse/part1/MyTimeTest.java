package oopcourse.part1;

public class MyTimeTest {
    public static void main(String[] args) {
        MyTime time1 = new MyTime();
        time1.printTime();

        MyTime time2 = new MyTime(555550000);
        time2.printTime();

        MyTime time3 = new MyTime(5, 23, 55);
        time3.printTime();
    }
}
