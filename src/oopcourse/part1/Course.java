package oopcourse.part1;

import java.util.Arrays;

public class Course {
    private String courseName;
    private String[] students = new String[100];
    private int numberOfStudents;

    public Course(String courseName) {
        this.courseName = courseName;
    }

    public void addStudent(String student) {
        if (numberOfStudents == this.students.length) {
            String[] newStudents = new String[numberOfStudents + 1];
            System.arraycopy(this.students, 0, newStudents, 0, numberOfStudents);
            this.students = newStudents;
        }
        students[numberOfStudents] = student;
        numberOfStudents++;
    }

    public String[] getStudents() {
        String[] students = new String[numberOfStudents];
        System.arraycopy(
                this.students, 0, students, 0, numberOfStudents);
        return students;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public String getCourseName() {
        return courseName;
    }

    public void dropStudent(String student) {
        int index = -1;
        for (int i = 0; i < this.students.length; i++) {
            if (this.students[i].equals(student)) {
                index = i;
                break;
            }
        }

        if (index < 0)
            return;

        for (int i = index; i < this.numberOfStudents - 1; i++) {
            this.students[i] = this.students[i + 1];
        }

        numberOfStudents--;
    }

    public void clear() {
        this.students = new String[100];
        numberOfStudents = 0;
    }
}
