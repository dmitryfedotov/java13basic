package oopcourse.part1;

public class MyDateTest {
    public static void main(String[] args) {
        MyDate myDate1 = new MyDate();
        System.out.println(myDate1);

        MyDate myDate2 = new MyDate(34355555133101L);
        System.out.println(myDate2);

        MyDate myDate3 = new MyDate(561555550000L);
        System.out.println(myDate3);
    }
}
