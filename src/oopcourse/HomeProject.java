package oopcourse;

import java.util.Scanner;

public class HomeProject {
    final static double ROUBLES_PER_KRONE = 5.88;
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int amountOfConvertation;
        do {
            System.out.print("Введите количество конвертаций: ");
            amountOfConvertation = input.nextInt();
        } while (amountOfConvertation <= 0);

        double[] roublesArray = new double[amountOfConvertation];

        for (int i = 0; i < amountOfConvertation; i++) {

            System.out.print("Введите сумму в кронах: ");
            int krone = input.nextInt();
            int digit; // Последняя цифра евро.

            System.out.print(krone);
            if (krone >= 5 && krone <= 20)
                System.out.print(" норвежских крон равны ");
            else {
                digit = krone % 10;
                if (digit == 1)
                    System.out.print(" норвежская крона равна ");
                else if (digit >= 2 && digit <= 4)
                    System.out.print(" норвежские кроны равны ");
                else
                    System.out.print(" норвежских крон равны ");
            }

            roublesArray[i] = findRoubles(krone);
            System.out.println((int) (roublesArray[i] * 100) / 100.0 + " российского рубля");
        }
    }

    public static void instruct() {
        System.out.println("Эта программа конвертирует сумму денег "
            + "из норвежских крон в российские рубли.");
        System.out.println("Курс покупки равен " + ROUBLES_PER_KRONE);
    }

    public static double findRoubles(double krone) {
        return krone * ROUBLES_PER_KRONE;
    }
}
