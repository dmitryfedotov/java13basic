package oopcourse;

import java.time.LocalDate;
import java.util.Scanner;

public class PercentsTask {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input month and year: ");
        int month = scanner.nextInt();
        int year = scanner.nextInt();

        System.out.print("Input length: ");
        int length = scanner.nextInt();

        System.out.print("Input rate: ");
        double rate = scanner.nextDouble();

        System.out.print("Input sum: ");
        double sum = scanner.nextDouble();

        int currentMonth = month;
        int currentYear = year;
        double totalSalary = 0;
        for (int i = 1; i <= length; i++) {
            currentMonth++;
            if (currentMonth == 13) {
                currentMonth = 1;
                currentYear++;
            }

            int monthLength = LocalDate.of(currentYear, currentMonth, 1).lengthOfMonth();
            double salary = sum * rate * monthLength / 365 / 100;
            System.out.printf("%s.%s salary is %s", currentYear, currentMonth, salary);
            System.out.println();
            totalSalary += salary;
        }
        System.out.printf("Total salary is %s", totalSalary);
    }
}
