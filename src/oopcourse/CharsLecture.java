package oopcourse;

public class CharsLecture {
    public static void main(String[] args) {
        System.out.println((int)'1');
        System.out.println((int)'A');
        System.out.println((int)'B');
        System.out.println((int)'a');
        System.out.println((int)'b');

        System.out.println((char)40);
        System.out.println((char)59);
        System.out.println((char)79);
        System.out.println((char)85);
        System.out.println((char)90);

        System.out.println((char)0x40);
        System.out.println((char)0x5A);
        System.out.println((char)0x71);
        System.out.println((char)0x72);
        System.out.println((char)0x7A);

        System.out.println((int)'a');
        System.out.println((int)'z');

        int number = 97 + (int) (Math.random() * (122 - 97 + 1));
        int number1= 97 + (int) (0.9999999999 * (122 - 97 + 1));
        System.out.println((char) number);
        System.out.println((char)4);
        System.out.println('z' - 'a');

        Character qwe = new Character('a');
        System.out.println("Java123".substring(0, 4));
    }
}
