package oopcourse.videocourse;

import java.util.List;

public class PecsTest {
    public static void main(String[] args) {

    }

    public static void extendsTest (List<? extends B> list) {
        System.out.println(list.get(0));
        B value = list.get(0);
        // list.add(new B()); // compilation error

    }

    public static void superTest(List<? super B> list) {
        System.out.println(list.get(0));
        //B value = list.get(0); // compilation error
        Object value1 = list.get(0);
        //list.add(list.get(0)); // compilation error
        list.add(new B());
        list.add(new C());
        //list.add(new A()); // compilation error
        //list.add(new Object()); // compilation error
    }

    static class A{};
    static class B extends A {};
    static class C extends B {};
}
