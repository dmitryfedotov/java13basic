package oopcourse.part2.abstractclasses.task9;

public class GeometricObjectTest {
    public static void main(String[] args) {
        Circle circle1 = new Circle(23);
        Rectangle rectangle1 = new Rectangle(10, 35);
        System.out.println(GeometricObject.max(circle1, rectangle1));
    }
}
