package oopcourse.part2.abstractclasses.task6;

import oopcourse.part2.exceptions.TriangleException;

import java.util.Scanner;

public class TriangleTest {
    public static void main(String[] args) throws TriangleException {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        String color = scanner.next();
        boolean filled = scanner.nextBoolean();

        Triangle triangle = new Triangle(a, b, c, color, filled);
        System.out.println(triangle.getArea());
        System.out.println(triangle.getPerimeter());
        System.out.println(triangle);
    }
}
