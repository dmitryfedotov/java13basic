package oopcourse.part2.abstractclasses.task6;

import homework.homework3part1.TriangleChecker;
import oopcourse.part1.geometric.GeometricObject;
import oopcourse.part2.exceptions.TriangleException;

public class Triangle extends GeometricObject {
    private double side1, side2, side3;

    public Triangle() throws TriangleException {
        this(1.0, 1.0, 1.0);
    }

    public Triangle(double side1, double side2, double side3) throws TriangleException {
        if (!TriangleChecker.triangleCanBeCreated(side1, side2, side3))
            throw new TriangleException();

        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public Triangle(double side1, double side2, double side3, String color, boolean filled) throws TriangleException {
        super(color, filled);
        if (!TriangleChecker.triangleCanBeCreated(side1, side2, side3))
            throw new TriangleException();

        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public double getSide1() {
        return side1;
    }

    public double getSide2() {
        return side2;
    }

    public double getSide3() {
        return side3;
    }

    @Override
    public double getArea() {
        double s = (side1 + side2 + side3) / 2;
        double area = Math.sqrt(s * (s - side1) * (s - side2) * (s - side3));

        return area;
    }

    @Override
    public double getPerimeter() {
        return side1 + side2 + side3;
    }

    @Override
    public String toString() {
        return "Треугольник: сторона1 = " + side1 + "; сторона2 = " + side2 +
                "; сторона3 = " + side3 + "; цвет = " + getColor()
                + "; заливка = " + isFilled();
    }
}
