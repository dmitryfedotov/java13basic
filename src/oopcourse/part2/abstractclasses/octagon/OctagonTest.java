package oopcourse.part2.abstractclasses.octagon;

import java.math.BigInteger;

public class OctagonTest {
    public static void main(String[] args) {
        Octagon octagon = new Octagon(5);
        System.out.println(octagon.getArea());
        System.out.println(octagon.getPerimeter());

        Octagon octagon1 = (Octagon) octagon.clone();
        System.out.println(octagon1.getArea());
        System.out.println(octagon1.getPerimeter());

    }
}
