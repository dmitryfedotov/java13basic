package oopcourse.part2.abstractclasses.octagon;

import oopcourse.part2.abstractclasses.task9.GeometricObject;

public class Octagon extends GeometricObject implements Cloneable {
    private double side;

    @Override
    public double getArea() {
        return (2 + 4 / Math.pow(2, 0.5)) * side * side;
    }

    @Override
    public double getPerimeter() {
        return side * 8;
    }

    public double getSide() {
        return side;
    }

    public Octagon() {
        this(1);
    }

    public Octagon(double side) {
        this.side = side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    protected Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
