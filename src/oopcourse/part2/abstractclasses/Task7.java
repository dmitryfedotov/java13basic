package oopcourse.part2.abstractclasses;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

public class Task7 {
    public static void main(String[] args) {
        ArrayList<Number> list = new ArrayList<>();
        list.add(45); // прибавляет целое число
        list.add(3445.53); // прибавляет число с плавающей точкой
        // Прибавляет BigInteger
        list.add(new BigInteger("3432323234344343101"));
        list.add(new BigInteger("3432323234344343102"));
        // Прибавляет BigDecimal
        list.add(new BigDecimal("2.0909090989091343433344343"));

        System.out.println(list);
        sort(list);
        System.out.println(list);

    }

    public static void sort(ArrayList<Number> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            int index = -1;
            BigDecimal min = new BigDecimal(list.get(i) + "");
            for (int j = i + 1; j < list.size(); j++) {
                BigDecimal currentMin = new BigDecimal(list.get(j) + "");
                if (currentMin.compareTo(min) == -1) {
                    min = currentMin;
                    index = j;
                }
            }
            if (index != -1) {
                Number tmp = list.get(i);
                list.set(i, list.get(index));
                list.set(index, tmp);
            }
        }
    }
}
