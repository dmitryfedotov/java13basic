package oopcourse.part2.abstractclasses.task11;

public interface Colorable {
    void howToColor();
}
