package oopcourse.part2.abstractclasses.task11;

import oopcourse.part2.abstractclasses.task9.GeometricObject;

public class Square extends GeometricObject implements Colorable {
    private double side;
    @Override
    public double getArea() {
        return side * side;
    }

    @Override
    public double getPerimeter() {
        return 4 * side;
    }

    @Override
    public void howToColor() {
        System.out.println("Раскрасьте все четыре стороны");
    }

    public Square() {
        super();
    }

    public Square(double side) {
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
}
