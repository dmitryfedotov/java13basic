package oopcourse.part2.abstractclasses.task11;

import oopcourse.part2.abstractclasses.task9.GeometricObject;

import java.util.ArrayList;
import java.util.List;

public class SquareTest {
    public static void main(String[] args) {
        List<GeometricObject> list = new ArrayList<>();
        list.add(new Square(123));
        list.add(new Square(1234));
        list.add(new Square(1235));
        list.add(new Square(1236));
        list.add(new Square(1237));

        for (GeometricObject element: list) {
            if (element instanceof Colorable)
                ((Colorable) element).howToColor();
        }
    }
}
