package oopcourse.part2.abstractclasses.task10;

import oopcourse.part1.geometric.Circle;

public class ComparableCircle extends Circle implements Comparable <Circle> {
    public ComparableCircle() {
        super();
    }

    public ComparableCircle(double radius) {
        super(radius);
    }

    public ComparableCircle(double radius, String color, boolean filled) {
        super(radius, color, filled);
    }

    @Override
    public int compareTo(Circle o) {
        return Double.compare(getArea(), o.getArea());
    }

    public static void main(String[] args) {
        ComparableCircle circle1 = new ComparableCircle(123);
        ComparableCircle circle2 = new ComparableCircle(223);

        if (circle1.compareTo(circle2) >= 0)
            System.out.println(circle1);
        else
            System.out.println(circle2);
    }
}
