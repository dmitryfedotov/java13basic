package oopcourse.part2.abstractclasses;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

public class Task8 {
    public static void main(String[] args) {
        ArrayList<Number> list = new ArrayList<>();
        list.add(45); // прибавляет целое число
        list.add(3445.53); // прибавляет число с плавающей точкой
        list.add(123123123);
        list.add(-123123123);
        list.add(-1);
        list.add(809802);
        // Прибавляет BigInteger
        list.add(new BigInteger("3432323234344343101"));
        list.add(new BigInteger("3432323234344343102"));
        // Прибавляет BigDecimal
        list.add(new BigDecimal("2.0909090989091343433344343"));

        System.out.println(list);
        shuffle(list);
        System.out.println(list);
    }

    public static void shuffle(ArrayList<Number> list) {
        int number = (int) (10.0 / Math.random());
        System.out.println(number);
        for (int i = 0; i < number; i++) {
            int index1 = (int) (Math.random() * list.size());
            int index2 = (int) (Math.random() * list.size());

            if (index1 != index2) {
                Number tmp = list.get(index1);
                list.set(index1, list.get(index2));
                list.set(index2, tmp);
            }
        }
    }
}
