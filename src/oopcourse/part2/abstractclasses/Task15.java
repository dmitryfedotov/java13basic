package oopcourse.part2.abstractclasses;

import oopcourse.part2.abstractclasses.task9.Circle;
import oopcourse.part2.abstractclasses.task9.GeometricObject;
import oopcourse.part2.abstractclasses.task9.Rectangle;

public class Task15 {
    public static void main(String[] args) {
        GeometricObject[] a = new GeometricObject[4];
        a[0] = new Circle(10);
        a[1] = new Circle(11);
        a[2] = new Rectangle(12, 13);
        a[3] = new Rectangle(121, 13);
        System.out.println(sumArea(a));
    }

    public static double sumArea(GeometricObject[] a) {
        double sum = 0;
        for (GeometricObject object: a) {
            sum += object.getArea();
        }

        return sum;
    }
}
