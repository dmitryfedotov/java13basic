package oopcourse.part2.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.print("Input two numbers: ");
            try {
                int a = scanner.nextInt();
                int b = scanner.nextInt();
                System.out.println(a + b);
                break;
            } catch (InputMismatchException e) {
                System.out.println("Do it again");
                scanner.nextLine();
            }
        } while (true);
    }
}
