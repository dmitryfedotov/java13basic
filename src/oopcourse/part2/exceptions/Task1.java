package oopcourse.part2.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        String[] months = {"январь", "февраль", "март", "апрель", "май",
                "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"};

        int[] dom = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        Scanner scanner = new Scanner(System.in);
        try {
            int monthNumber = scanner.nextInt();
            System.out.println(months[monthNumber - 1] + ": " + dom[monthNumber - 1]);
        } catch (InputMismatchException | ArrayIndexOutOfBoundsException e) {
            System.out.println("Недопустимое число");
        }

    }
}
