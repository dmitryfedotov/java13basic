package oopcourse.part2.generics.task22;

import java.util.ArrayList;

public class GenericStack<E> extends ArrayList<E> {
    public int getSize() {
        return size();
    }

    public E peek() {
        return get(getSize() - 1);
    }

    public void push(E o) {
        add(o);
    }

    public E pop() {
        E o = (E) get(getSize() - 1);
        remove(getSize() - 1);
        return o;
    }

    @Override
    public String toString() {
        return "стек: " + super.toString();
    }
}