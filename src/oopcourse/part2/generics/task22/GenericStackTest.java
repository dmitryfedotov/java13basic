package oopcourse.part2.generics.task22;

public class GenericStackTest {
    public static void main(String[] args) {
        GenericStack<String> stack = new GenericStack<>();
        System.out.println(stack.getSize());
        stack.push("111");
        stack.push("222");
        stack.push("333");
        stack.push("333");
        stack.push("444");
        stack.push("555");
        stack.push("666");
        stack.push("777");
        stack.push("888");
        stack.push("999");
        stack.push("000");
        stack.push("111111");
        System.out.println(stack.getSize());

        System.out.println(stack.peek());
        System.out.println(stack.pop());
        System.out.println(stack.getSize());
    }
}
