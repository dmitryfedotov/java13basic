package oopcourse.part2.generics;

import oopcourse.part2.abstractclasses.task9.Circle;

public class Task25 {
    public static void main(String[] args) {
        Circle[] circles = new Circle[5];
        circles[0] = new Circle(5);
        circles[1] = new Circle(7);
        circles[2] = new Circle(3);
        circles[3] = new Circle(99);
        circles[4] = new Circle(88);

        Circle max = circles[0];
        for (int i = 1; i < circles.length; i++) {
            if (max.compareTo(circles[i]) < 0)
                max = circles[i];
        }

        max.printCircle();
    }
}
