package oopcourse.part2.generics.task21;

import java.util.Arrays;

public class GenericStack<E> {
    private java.util.ArrayList<E> listOld = new java.util.ArrayList<>();
    private E[] list = (E[]) new Object[10];
    private int size = 0;

    public int getSize() {
        return size;
    }

    public E peek() {
        return list[getSize() - 1];
    }

    public void push(E o) {
        if (getSize() == list.length) {
            E[] newList = (E[]) new Object[list.length * 2];
            System.arraycopy(list, 0, newList, 0, list.length);
            list = newList;
        }
        list[size++] = o;
    }

    public E pop() {
        E o = list[size - 1];
        list[--size] = null;
        return o;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public String toString() {
        return "стек: " + Arrays.toString(list);
    }
}