package oopcourse.part2.generics;

import java.util.ArrayList;

public class Task24 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(6);

        System.out.println(getIndex(list, 4));
        System.out.println(getIndex(list, 5));
    }

    public static <T> int getIndex(ArrayList<T> list, T element) {
        int index = -1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(element)) {
                index = i;
                break;
            }
        }
        return index;
    }
}
