package oopcourse.part2.generics;

public class Task26 {
    public static void main(String[] args) {
        int[][] matrix = new int[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                matrix[i][j] = (int) (Math.random() * 1000);
            }
        }

        Integer max = Integer.MIN_VALUE;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (max.compareTo(matrix[i][j]) < 0)
                    max = matrix[i][j];
            }
        }

        System.out.println(max);
    }
}
