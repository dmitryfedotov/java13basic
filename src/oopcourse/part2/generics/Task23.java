package oopcourse.part2.generics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Task23 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(1);
        list.add(2);
        list.add(2);
        list.add(3);
        list.add(4);
        System.out.println(getUniqueElements(list));
    }

    public static <T> ArrayList<T> getUniqueElements(ArrayList<T> list) {
        ArrayList<T> newList = new ArrayList<>();

        for (T element: list) {
            if (!newList.contains(element))
                newList.add(element);
        }

        return newList;
    }
}
