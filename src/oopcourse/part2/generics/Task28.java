package oopcourse.part2.generics;

import java.util.ArrayList;

public class Task28 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(4);
        list.add(1);
        list.add(2);
        list.add(-1);
        list.add(9);
        sort(list);
        System.out.println(list);
    }

    public static <E extends Comparable<E>> void sort(ArrayList<E> list) {
        for (int i = 0; i < list.size(); i++) {
            E min = list.get(i);
            int index = -1;
            for (int j = i + 1; j < list.size(); j++) {
                if (min.compareTo(list.get(j)) > 0) {
                    min = list.get(j);
                    index = j;
                }
            }

            if (index != -1) {
                E tmp = list.get(i);
                list.set(i, min);
                list.set(index, tmp);
            }
        }
    }
}
