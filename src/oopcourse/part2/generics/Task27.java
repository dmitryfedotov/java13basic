package oopcourse.part2.generics;

import java.util.ArrayList;

public class Task27 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < 50; i++) {
            list.add(i);
        }

        System.out.println(list);
        shuffle(list);
        System.out.println(list);
    }

    public static <T> void shuffle(ArrayList<T> list) {
        for (int i = 0; i < list.size(); i++) {
            int newIndex = (int) (Math.random() * list.size());

            T tmp = list.get(i);
            list.set(i, list.get(newIndex));
            list.set(newIndex, tmp);
        }
    }
}
