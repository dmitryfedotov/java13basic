package oopcourse;

import java.util.Scanner;

public class SalaryCounting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int sum = 0;
        for (int i = 1; i <= n; i++) {
            sum += i;
        }

        System.out.println(sum);
        if (sum == n * (n + 1) / 2)
            System.out.println("Равны");
        else
            System.out.println("Не равны");
    }
}
