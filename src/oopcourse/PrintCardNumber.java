package oopcourse;

import java.util.Scanner;

public class PrintCardNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input card number: ");
        String cardNumber = scanner.nextLine();

        printNumber(cardNumber, 0);
    }

    public static void printNumber(String cardNumber, int currentPosition) {
        if (cardNumber.length() - currentPosition <= 4)
            System.out.print(cardNumber.substring(currentPosition));
        else {
            System.out.print("*");
            printNumber(cardNumber, currentPosition + 1);
        }
    }
}
