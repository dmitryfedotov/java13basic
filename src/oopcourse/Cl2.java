package oopcourse;

public class Cl2

{
    public static void main(String[] args) {
        BB bb = new BB();
        BB bb1 = new BB(1);
    }
}

class AA{
    public AA() {
        System.out.println("oopcourse.AA");
    }

    public AA(int n) {
        System.out.println(n + "oopcourse.AA");
    }
}

class BB extends AA{
    public BB() {
        System.out.println("oopcourse.BB");
    }

    public BB(int n) {
        System.out.println(n + "oopcourse.BB");
    }
}