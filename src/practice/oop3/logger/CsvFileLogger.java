package practice.oop3.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class CsvFileLogger extends FileNameHandler implements Logger {
    private String fileName = "output";
    private static final String csvExtension = ".csv";

    public CsvFileLogger(String filename) {
        this.fileName = filename;
    }

    public CsvFileLogger() {
        this.fileName = getDefaultFileName();
    }

    @Override
    public void log(String message) {
        try (Writer writer = new FileWriter(fileName + csvExtension)){
            writer.write(message + "\n");
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }

}
