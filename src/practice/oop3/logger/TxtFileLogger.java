package practice.oop3.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class TxtFileLogger implements Logger {
    private String fileName = "output";
    @Override
    public void log(String message) {
        try (Writer writer = new FileWriter(fileName + ".txt")){
            writer.write(message + "\n");
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }
}
