package practice.oop3.logger;

public interface Logger {
    void log(String message);

    default void smth() {
        System.out.println("Default method");
    }
    // маркерные интерфейсы не имеют методов.

}
