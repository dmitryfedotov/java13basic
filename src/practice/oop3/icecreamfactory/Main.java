package practice.oop3.icecreamfactory;

public class Main {
    public static void main(String[] args) {
        IceCream cherryIceCream = IceCreamFactory.getIceCream(IceCreamType.CHERRY);
        //IceCream cherryIceCream1 = IceCreamFactory.getIceCream(VanillaIceCream.class);

        cherryIceCream.printIngredients();
        //cherryIceCream1.printIngredients();
    }
}
