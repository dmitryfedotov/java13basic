package practice.oop3.icecreamfactory;

public interface IceCream {
    void printIngredients();
}
