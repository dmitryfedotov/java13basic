package practice.oop3.icecreamfactory;

public class IceCreamFactory {

    private IceCreamFactory(){}
    public static IceCream getIceCream(IceCreamType type) {
        IceCream iceCream = null;
        switch (type) {
            case CHERRY -> iceCream = new CherryIceCream();
            case VANILLA -> iceCream = new ChocolateIceCream();
            case CHOCOLATE -> iceCream = new VanillaIceCream();
        }

        return iceCream;
    }

    public static IceCream getIceCream(Class<IceCream> obj) {
        IceCream iceCream = null;

        if (obj.equals(CherryIceCream.class))
            iceCream = new CherryIceCream();
        else if (obj.equals(ChocolateIceCream.class))
            iceCream = new ChocolateIceCream();
        else if (obj.equals(VanillaIceCream.class))
            iceCream = new VanillaIceCream();

        return iceCream;
    }

}
