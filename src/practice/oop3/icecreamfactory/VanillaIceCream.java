package practice.oop3.icecreamfactory;

public class VanillaIceCream implements IceCream {
    @Override
    public void printIngredients() {
        System.out.println("Vanilla, ice, cream, love, stevia");
    }
}
