package practice.oop3.icecreamfactory;

public class ChocolateIceCream implements IceCream {
    @Override
    public void printIngredients() {
        System.out.println("Chocolate, ice, cream, love");
    }
}
