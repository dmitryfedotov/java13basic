package practice.oop3.arrayexample;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("AUDI", "1999"));
        cars.add(new Car("BMW", "1990"));
        cars.add(new Car("VOLVO", "2022"));

        cars.forEach(System.out::println);

        // java.util.ConcurrentModificationException
//        for (Car car: cars) {
//            if (car.getModel().equals("VOLVO"))
//                cars.remove(car);
//        }

        Iterator<Car> iterator = cars.iterator();
        while (iterator.hasNext()) {
            Car car = iterator.next();
            if (car.getModel().equals("VOLVO")) {
                iterator.remove();
            }
        }

        System.out.println(cars);

        Collections.sort(cars, new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return o1.getModel().compareTo(o2.getModel());
            }
        });

        Collections.sort(cars, (o1, o2) -> o1.getModel().compareTo(o2.getModel()));

        Collections.sort(cars, Comparator.comparing(Car::getModel));
    }
}
