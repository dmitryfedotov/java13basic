package practice.oop2.task2;

import java.io.*;
import java.util.Scanner;

public class ReadFile {
    private final static String FOLDER_DIRECTORY = "C:\\Users\\Дмитрий\\IdeaProjects\\java13basic\\src\\week8\\oop2\\task2\\file";
    private final static String OUTPUR_FILE_NAME = "output.txt";

    private ReadFile() {

    }

    public static void readDataFromFile(String filePath) throws IOException {
        Scanner scanner = new Scanner(new File(filePath));
        String[] days = new String[10];
        int i = 0;
        while (scanner.hasNext())
            days[i++] = scanner.next();

        Writer writer = new FileWriter(FOLDER_DIRECTORY + "\\" + OUTPUR_FILE_NAME);
        for (int j = 0; j < i; j++) {
            //System.out.println(String.format("Порядковый номер дня недели %s = %s", days[j], WeekDays.ofName(days[j]).dayNumber));
            writer.write(String.format("Порядковый номер дня недели %s = %s \n", days[j], WeekDays.ofName(days[j]).dayNumber));
        }

        // example try with resources
        // you don't need to close stream manually
//        try (Writer writer1 = new FileWriter("")){
//            ...
//        }

        writer.close();
        scanner.close();
    }

    public static void readDataFromFile() throws IOException {
        readDataFromFile(FOLDER_DIRECTORY + "\\input.txt");
    }
}
