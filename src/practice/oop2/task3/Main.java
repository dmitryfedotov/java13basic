package practice.oop2.task3;

public class Main {
    public static void main(String[] args) {
        SimpleArrayList simpleArrayList = new SimpleArrayList();
        simpleArrayList.add(12);
        simpleArrayList.add(13);
        simpleArrayList.add(15);
        simpleArrayList.add(16);
        simpleArrayList.add(17);
        simpleArrayList.add(18);
        System.out.println(simpleArrayList.size());
        System.out.println(simpleArrayList.get(0));
        System.out.println(simpleArrayList.get(1));
        System.out.println(simpleArrayList.get(2));
        System.out.println(simpleArrayList.get(simpleArrayList.size() - 1));
        System.out.println(simpleArrayList.get(simpleArrayList.size()));
    }
}
