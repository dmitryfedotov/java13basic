package practice.oop2.task3;

/*
Примитивная реализация ArrayList.
Массив только int, из методов только добавлять элемент,
получать size и увеличивать капасити, когда добавляется новый.
 */

import java.util.Arrays;

public class SimpleArrayList {
    private int size;
    private int[] array;
    private int capacity;
    private static final int DEFAULT_CAPACITY = 5;
    private int currentIndex;

    public SimpleArrayList() {
        array = new int[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        size = 0;
        currentIndex = 0;
    }

    public SimpleArrayList(int size) {
        array = new int[size];
        capacity = size;
        size = 0;
        currentIndex = 0;
    }

    public void add(int element) {
        if (currentIndex >= capacity) {
            capacity *= 2;
            array = Arrays.copyOf(array, capacity);
        }

        array[currentIndex] = element;
        size++;
        currentIndex++;
    }

    public int get(int index) {
        if (index < 0 || index >= size) {
            System.out.println("Неправильный индекс");
            return -1;
        } else {
            return array[index];
        }
    }

    public int size() {
        return size;
    }
}
