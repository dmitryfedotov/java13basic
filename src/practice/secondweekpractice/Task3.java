package practice.secondweekpractice;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        String str = "" + n;
        if (str.charAt(0) == str.charAt(3) && str.charAt(1) == str.charAt(2))
            System.out.println("палиндром");
        else
            System.out.println("не палиндром");
    }
}
