package practice.secondweekpractice;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input number: ");
        int n = scanner.nextInt();

        if (n % 2 == 0) {
            String str;
            if (n >= 0)
                str = "Четное больше или равно нулю";
            else
                str = "Четное меньше нуля";
            System.out.println(str);
        }
    }
}
