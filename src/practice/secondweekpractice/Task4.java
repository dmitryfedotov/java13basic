package practice.secondweekpractice;

import java.util.Scanner;

public class Task4 {
    final static int VIP_PRICE = 125;
    final static int PREMIUM_PRICE = 120;
    final static int STANDARD_PRICE = 110;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int type = scanner.nextInt();
//        if (type == 1)
//            System.out.println(VIP_PRICE);
//        else if (type == 2)
//            System.out.println(PREMIUM_PRICE);
//        else if (type == 3)
//            System.out.println(STANDARD_PRICE);
//        else
//            System.out.println("Wrong type");
//        switch (type) {
//            case 1:
//                System.out.println(VIP_PRICE);
//                break;
//            case 2:
//                System.out.println(PREMIUM_PRICE);
//                break;
//            case 3:
//                System.out.println(STANDARD_PRICE);
//                break;
//            default:
//                System.out.println("Wrong type");
//                break;
//        }

        switch (type) {
            case 1 -> System.out.println(VIP_PRICE);
            case 2 -> System.out.println(PREMIUM_PRICE);
            case 3 -> System.out.println(STANDARD_PRICE);
            default -> System.out.println("Wrong type");
        }
    }
}
