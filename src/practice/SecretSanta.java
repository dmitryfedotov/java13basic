package practice;

import java.util.*;

public class SecretSanta {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();

        List<Integer> numbers = new ArrayList<>();
        Map<Integer, Integer> participants = new HashMap<>();

        for (int i = 2; i <= count; i++) {
            numbers.add(i);
        }

        int currentSender = 1;
        while (numbers.size() > 0) {
            Integer currentReceiver = numbers.get((int) (Math.random() * numbers.size()));
            participants.put(currentSender, currentReceiver);
            numbers.remove(currentReceiver);
            currentSender = currentReceiver;
        }
        participants.put(currentSender, 1);

        for (Map.Entry<Integer, Integer> entry: participants.entrySet()) {
            System.out.println(entry.getKey() + " -> " + entry.getValue());
        }
    }
}
