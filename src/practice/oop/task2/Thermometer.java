package practice.oop.task2;

public class Thermometer {
    private double tempCelsius;
    private double tempFahrenheit;

    public Thermometer(double temperature, TemperatureUnit temperatureUnit) {
        if (temperatureUnit == TemperatureUnit.CELSIUS) {
            this.tempCelsius = temperature;
            this.tempFahrenheit = fromCelsiusToFahrenheit(temperature);
        } else if (temperatureUnit == TemperatureUnit.FAHRENHEIT) {
            this.tempFahrenheit = temperature;
            this.tempFahrenheit = fromFahrenheitToCelsius(temperature);
        } else {
            System.out.println("Температура не распознана");
            this.tempCelsius = temperature;
            this.tempFahrenheit = fromCelsiusToFahrenheit(temperature);
        }
    }

    public double getTempCelsius() {
        return tempCelsius;
    }

    public double getTempFahrenheit() {
        return tempFahrenheit;
    }

    private double fromCelsiusToFahrenheit(double currentTemperature) {
        return currentTemperature * 1.8 + 32;
    }

    private double fromFahrenheitToCelsius(double currentTemperature) {
        return (currentTemperature - 32) / 1.8;
    }
}
