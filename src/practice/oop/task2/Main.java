package practice.oop.task2;

public class Main {
    public static void main(String[] args) {
        Thermometer thermometer = new Thermometer(-25, TemperatureUnit.CELSIUS);
        System.out.println(thermometer.getTempCelsius());
        System.out.println(thermometer.getTempFahrenheit());
    }
}
