package practice.oop.task2;

public enum TemperatureUnit {
    CELSIUS,
    FAHRENHEIT
}
