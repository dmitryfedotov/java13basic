package practice.oop.task4;

public class Robot {
    private int x, y;
    private int direction; //0 -- up, 1 -- right, 2 -- bottom, 3 -- left

    public Robot() {
        this.x = 0;
        this.y = 0;
        this.direction = 0;
    }

    public Robot(int x, int y) {
        this.x = x;
        this.y = y;
        this.direction = 0;
    }

    public Robot(int x, int y, int direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public void go() {
        switch (direction) {
            case 0 -> this.y++;
            case 1 -> this.x++;
            case 2 -> this.y--;
            case 3 -> this.x--;
        }
    }

    public void turnLeft() {
        this.direction = (this.direction - 1) % 4;
    }

    public void turnRight() {
        this.direction = (this.direction + 1) % 4;
    }

    public void printCoordinates() {
        System.out.println("(x, y): " + x + ", " + y);
    }
}
