package practice.oop.task3;

public class Main {
    public static void main(String[] args) {
        System.out.println(FieldValidator.validateEmail("qwe123@qwe.com"));
        System.out.println(FieldValidator.validateEmail("qwe123@qwe.co"));

        System.out.println(FieldValidator.validateDate("12.23.2222"));
        System.out.println(FieldValidator.validateDate("121.23.2222"));

        System.out.println(FieldValidator.validateName("Qwerwerwer"));
        System.out.println(FieldValidator.validateName("werwerwer"));

        System.out.println(FieldValidator.validatePhone("+12345678901"));
        System.out.println(FieldValidator.validatePhone("+123456789011"));
    }
}
