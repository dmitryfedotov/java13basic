package practice.oop.task1;

public class Main {
    public static void main(String[] args) {
        Bulb bulb = new Bulb();
        System.out.println(bulb.isShining());
        bulb.turnOn();
        System.out.println(bulb.isShining());

        Chandelier chandelier = new Chandelier(4);
        System.out.println(chandelier.isShining());
        chandelier.turnOn();
        System.out.println(chandelier.isShining());
    }
}
