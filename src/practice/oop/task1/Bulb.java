package practice.oop.task1;

public class Bulb {
    private boolean toggle;

    public Bulb() {
        toggle = false;
    }

    public Bulb(boolean condition) {
        this.toggle = condition;
    }

    public void turnOn() {
        this.toggle = true;
    }

    public void turnOff() {
        this.toggle = false;
    }

    public boolean isShining() {
        return this.toggle;
    }
}
