package practice.oop.task1;

public class Chandelier {
    private Bulb[] bulbs;

    public Chandelier(int countOfBulbs) {
        bulbs = new Bulb[countOfBulbs];
        for (int i = 0; i < countOfBulbs; i++) {
            bulbs[i] = new Bulb();
        }
    }

    public void turnOn() {
        for (Bulb bulb: bulbs) {
            bulb.turnOn();
        }
    }

    public void turnOff() {
        for (Bulb bulb: bulbs) {
            bulb.turnOff();
        }
    }

    public boolean isShining() {
        return bulbs[0].isShining();
    }

}
