package practice.firstweekpractice;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите координаты x и y:");
        double x = scanner.nextDouble();
        double y = scanner.nextDouble();

        double d = Math.sqrt(x * x + y * y);
        System.out.printf("Расстояние от центра до точки: %s", d);
    }
}
