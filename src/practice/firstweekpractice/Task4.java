package practice.firstweekpractice;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите площадь круга:");
        double area = scanner.nextDouble();

        double radius = Math.sqrt(area / Math.PI);
        System.out.printf("Радиус равен %s", radius);
        System.out.println();
        System.out.printf("Длина окружности равна %s", 2 * Math.PI * radius);
    }
}
