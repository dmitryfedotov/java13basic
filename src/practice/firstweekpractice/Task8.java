package practice.firstweekpractice;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        int x = scanner.nextInt();
        int next = x + 2 - (x % 2);
        System.out.printf("Следующее четное число %s", next);
    }
}
