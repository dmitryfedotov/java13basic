package practice.firstweekpractice;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите двузначное число: ");
        int x = scanner.nextInt();

        System.out.printf("Десятки - %s, единицы - %s", x / 10, x % 10);
    }
}
