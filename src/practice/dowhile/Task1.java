package practice.dowhile;

public class Task1 {
    public static void main(String[] args) {
        for (int i = 0; i < 11; i++) {
            int j = 0;
            do {
                System.out.print(j + " ");
                j++;
            } while (j <= i && i + j < 11);
//            for (int j = 0; j < i && i + j < 12; j++) {
//                System.out.print(j + " ");
//            }
            System.out.println();
        }
    }
}
