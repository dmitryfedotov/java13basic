package practice.fourthweekpractice;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int max1 = Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;

        for (int i = 0; i < n; i++) {
            int currentNumber = scanner.nextInt();
            if (currentNumber > max1) {
                max2 = max1;
                max1 = currentNumber;
            }
            else if (currentNumber > max2)
                max2 = currentNumber;
        }

        System.out.println(max1);
        System.out.println(max2);
    }
}
