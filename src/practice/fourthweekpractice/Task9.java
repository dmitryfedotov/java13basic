package practice.fourthweekpractice;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int result = 0;
        while (true) {
            if (scanner.nextInt() < 0)
                result++;
            else
                break;
        }
        System.out.println(result);
    }
}
