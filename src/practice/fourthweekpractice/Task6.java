package practice.fourthweekpractice;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int fib1 = 0;
        int fib2 = 1;
        for (int i = 2; i <= n; i++) {
            int tmp = fib2;
            fib2 += fib1;
            fib1 = tmp;
        }

        System.out.println(fib2);
    }
}
