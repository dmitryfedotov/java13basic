package practice.fourthweekpractice;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int m = scanner.nextInt();
        int n = scanner.nextInt();

        for (int i = 0; i <= n; i++) {
            int pow = (int)Math.pow(m, i);
            System.out.println(pow);
        }

        int result = 1;
        for (int i = 0; i <= n; i++) {
            System.out.println(result);
            result *= m;
        }

        System.out.println(1);
        if (n > 0)
            System.out.println(m);

        if (n > 1) {
            result = m;
            for (int i = 2; i <= n; i++) {
                for (int j = 1; j < m; j++) {
                    result += result;
                }
                System.out.println(result);
            }
        }
    }
}
