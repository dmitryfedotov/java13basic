package practice.fourthweekpractice;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int percent = scanner.nextInt();

        double deposit = 1000;
        int month = 0;
        while (deposit < 1100) {
            deposit *= (double) (100 + percent) / 100;
            month++;
        }

        System.out.println(month);
        System.out.println(deposit);
    }
}
