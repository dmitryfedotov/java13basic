package practice.fifthweekpractice;

import java.util.Arrays;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }

        int m = scanner.nextInt();
        int[] b = new int[m];
        for (int i = 0; i < m; i++) {
            b[i] = scanner.nextInt();
        }

        int indexA = 0;
        int indexB = 0;
        int[] result = new int[n + m];
        for (int i = 0; i < n + m; i++) {
            if (indexB == m || indexA < n && a[indexA] <= b[indexB] ) {
                result[i] = a[indexA];
                indexA++;
            } else {
                result[i] = b[indexB];
                indexB++;
            }
        }

        System.out.println(Arrays.toString(result));
    }
}
