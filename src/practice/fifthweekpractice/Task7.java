package practice.fifthweekpractice;

/*
  На вход подается число N - длина массива.
     Затем передается массив строк длины N.
     После этого - число M.

     Сохранить в другом массиве только те элементы, длина строки которых
     не превышает M.

     Входные данные:
     5
     Hello
     good
     to
     see
     you
     4

     Выходные данные:
     good to see you
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        String[] strings = new String[n];
        int i = 0;
        while (i < n) {
            String currentString = scanner.nextLine();
            if (!currentString.equals("")) {
                strings[i++] = currentString;
            }
        }

        int m = scanner.nextInt();
        for (i = 0; i < n; i++) {
            if (strings[i].length() <= m)
                System.out.print(strings[i] + " ");
        }
    }
}
