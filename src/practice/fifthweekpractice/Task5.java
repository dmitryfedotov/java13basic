package practice.fifthweekpractice;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }

        int m = scanner.nextInt();
        boolean flag = false;

        for (int i = 0; i < n - 1; i++) {
            if (a[i] > m)
                continue;
            for (int j = i + 1; j < n; j++) {
                if (a[i] + a[j] == m) {
                    System.out.println(a[i] + " " + a[j]);
                    flag = true;
                }
            }
        }

        if (!flag)
            System.out.println(-1);
    }
}
