package practice.fifthweekpractice;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];

        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }

        boolean result = true;
        for (int i = 0; i < n - 1; i++) {
            if (a[i] <= a[i + 1]) {
                result = false;
                break;
            }
        }

        System.out.println(result);
    }
}
