package practice.thirdweekpractice;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        String inputColor = new Scanner(System.in).nextLine();

        System.out.println(inputColor.matches("#[0-9A-Fa-f]{6}"));
    }
}
