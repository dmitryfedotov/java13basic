package practice.thirdweekpractice;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String cardNumber = scanner.nextLine();
        String pin = scanner.nextLine();

        boolean numberIsValid = cardNumber.matches("(\\d{4} ){3}\\d{4}");
        boolean pinIsValid = pin.matches("\\d{4}");

        System.out.printf("Card number is valid: %s. PIN is valid: %s", numberIsValid, pinIsValid);
    }
}
